-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2015 at 01:59 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toyota_db_dns`
--

-- --------------------------------------------------------

--
-- Table structure for table `fog_pass`
--

CREATE TABLE IF NOT EXISTS `fog_pass` (
  `f_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_email` varchar(50) NOT NULL,
  `request` int(10) NOT NULL,
  `req_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`f_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `fog_pass`
--

INSERT INTO `fog_pass` (`f_id`, `u_email`, `request`, `req_dt`) VALUES
(5, 'ms@mail.com', 1, '2014-06-23 06:19:07'),
(14, 'irene.mwihaki@toyotakenya.com', 1, '2015-07-29 13:28:56'),
(13, 'irene.mwihaki@toyotakenya.com', 1, '2015-07-29 13:27:51');

-- --------------------------------------------------------

--
-- Table structure for table `fog_pass_ad`
--

CREATE TABLE IF NOT EXISTS `fog_pass_ad` (
  `f_id_adm` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ad_email` varchar(50) NOT NULL,
  `request` int(10) NOT NULL,
  `req_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`f_id_adm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_admin`
--

CREATE TABLE IF NOT EXISTS `report_admin` (
  `rp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_id` int(10) NOT NULL,
  `sys_id` int(10) NOT NULL,
  `u_em` varchar(50) NOT NULL,
  `rep_state` varchar(255) NOT NULL,
  `rp_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `report_admin`
--

INSERT INTO `report_admin` (`rp_id`, `u_id`, `sys_id`, `u_em`, `rep_state`, `rp_date`) VALUES
(4, 1, 1, 'sifu@mail.com', 'therer u go..!!!!', '2014-06-03 15:24:48'),
(5, 1, 1, 'sifu@mail.com', 'did u notice this..??', '2014-06-05 10:15:59'),
(6, 26, 1, 'ms@mail.com', 'is this ok', '2014-06-11 04:00:16'),
(7, 25, 6, 'sm@mail.com', 'did u find this offenxive', '2014-06-11 04:57:40');

-- --------------------------------------------------------

--
-- Table structure for table `report_user`
--

CREATE TABLE IF NOT EXISTS `report_user` (
  `u_rp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_id` int(10) NOT NULL,
  `u_email` varchar(100) NOT NULL,
  `rep_state` varchar(255) NOT NULL,
  `rp_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`u_rp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `report_user`
--

INSERT INTO `report_user` (`u_rp_id`, `sys_id`, `u_email`, `rep_state`, `rp_date`) VALUES
(3, 0, 'sifu@mail.com', 'ok now!!', '2014-06-03 12:49:08'),
(4, 0, 'sifu@mail.com', 'if this is ok i would gladly do it...', '2014-06-05 07:12:45'),
(5, 0, 'sifu@mail.com', 'is that clear enough!!', '2014-06-05 09:57:52'),
(6, 0, 'sifu@mail.com', 'does this look ok!!!', '2014-06-05 10:09:52'),
(7, 0, 'sifu@mail.com', 'now!!!', '2014-06-05 10:11:50'),
(8, 404, 'pips@mail.com', 'i need some help!!!', '2014-06-05 14:41:02'),
(9, 404, 'ms@mail.com', 'is this ok', '2014-06-11 00:54:43'),
(10, 6, 'sm@mail.com', 'try', '2014-07-22 08:50:45'),
(11, 8, 'service@tk.com', 'Hi Admin!!!', '2015-05-18 09:26:10'),
(12, 8, 'service@tk.com', 'Hi There...;-)', '2015-05-18 09:36:02');

-- --------------------------------------------------------

--
-- Table structure for table `sysadmin`
--

CREATE TABLE IF NOT EXISTS `sysadmin` (
  `sys_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fn` varchar(20) NOT NULL,
  `ln` varchar(20) NOT NULL,
  `em` varchar(100) NOT NULL,
  `ai` varchar(30) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `dep` varchar(50) NOT NULL,
  `pro_pic` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `log` date NOT NULL DEFAULT '0000-00-00',
  `nav` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`sys_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `sysadmin`
--

INSERT INTO `sysadmin` (`sys_id`, `fn`, `ln`, `em`, `ai`, `salt`, `dep`, `pro_pic`, `pwd`, `log`, `nav`) VALUES
(1, 'System', 'Admin', 'super.admin@tk.com', 'CNU92', '1627088588', 'superadmin', '98b0422c9fbe6d0993252e74d324f375.png', '5f4dcc3b5aa765d61d8327deb882cf99', '2014-02-26', '2014-02-26'),
(7, 'Mildred', 'Njuguna', 'admin.gov@tk.com', '26', '6959147465', 'Business Planning And Government', 'admin.gov@tk.com_avatar.png', '3e22049ecb056a49e1ab965a01bf9379', '2015-05-15', '2015-05-15'),
(8, 'Linner', 'Mutai', 'linah.mutai@toyotakenya.com', '001', '1357015667', 'Services', 'linah.mutai@toyotakenya.com_avatar.png', '73f0d0d9075f0b8b15c0e6a011fbed4f', '2015-05-15', '2015-05-15'),
(9, 'Wanjiru', 'Waiyaki', 'wanjiru.waiyaki@toyotakenya.com', '1559', '1735763655', 'Human Resources', 'wanjiru.waiyaki@toyotakenya.com_avatar.png', '2c869eba455f428ceac0c528c7514d16', '2015-05-15', '2015-05-15'),
(10, 'mdo', 'mdo', 'mdo@tk.com', 'CNU92', '8116014245', 'MDs Office', 'mdo@tk.com_avatar.png', '5f4dcc3b5aa765d61d8327deb882cf99', '2015-05-15', '2015-05-15'),
(11, 'edward', 'liyayi', 'edward.liyayi@toyotakenya.com', '3', '4232865985', 'Services', 'edward.liyayi@toyotakenya.com_avatar.png', '6089f54e830cc8d0fecd38f0b045ffa6', '2015-05-15', '2015-05-15'),
(12, 'Justice', 'Andiwo', 'justice.andiwo@toyotakenya.com', '29590', '1509323197', 'IT', 'justice.andiwo@toyotakenya.com_avatar.png', 'c53e479b03b3220d3d56da88c4cace20', '2015-05-18', '2015-05-18'),
(13, 'Nehemiah', 'Mutai', 'nehemiah.mutai@toyotakenya.com', '333', '3272817485', 'Parts', 'nehemiah.mutai@toyotakenya.com_avatar.png', '17c7e2c490ef8537dff17e5949589682', '2015-05-18', '2015-05-18'),
(14, 'Paul', 'Maunda', 'paul.maunda@toyotakenya.com', '1976', '1123556138', 'Services', 'paul.maunda@toyotakenya.com_avatar.png', 'cfeee3fc113367fb6ea82084621e5c0d', '2015-05-18', '2015-05-18'),
(15, 'Sundeep', 'Rupra', 'sundeep.hunjan@toyotakenya.com', '2010', '8645201765', 'Vehicle Sales', 'sundeep.hunjan@toyotakenya.com_avatar.png', '10cc9daa5c1eecd5e344adf95260a188', '2015-05-18', '2015-05-18'),
(16, 'irene', 'mwihaki', 'irene.mwihaki@toyotakenya.com', '2010', '2057001141', 'Services', 'irene.mwihaki@toyotakenya.com_avatar.png', '3c24fe2808269610982031e445350db1', '2015-05-18', '2015-05-18'),
(17, 'uwe', '  Lehmgrubler', 'uwe.lehmgrubler@toyotakenya.com', '13', '1151484780', 'superadmin', 'uwe.lehmgrubler@toyotakenya.com_avatar.png', 'e6e0ec4af7c47316e06502f97bf9e26c', '2015-07-29', '2015-07-29'),
(18, 'Damiana', 'Mueni', 'damiana.mueni@toyotakenya.com', '2828', '1608215023', 'Customer Relations', 'damiana.mueni@toyotakenya.com_avatar.png', 'e9403a3edf96ef7be8102b61c385f621', '2015-07-29', '2015-07-29');

-- --------------------------------------------------------

--
-- Table structure for table `syscat`
--

CREATE TABLE IF NOT EXISTS `syscat` (
  `ct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat` varchar(50) NOT NULL,
  `dt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ct_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `syscat`
--

INSERT INTO `syscat` (`ct_id`, `cat`, `dt`) VALUES
(32, 'MDs Office', '2014-06-11 02:13:21'),
(5, 'Services', '2014-03-04 20:00:23'),
(7, 'Human Resources', '2014-03-04 20:01:48'),
(8, 'Customer Relations', '2014-03-04 20:02:16'),
(9, 'Body Shop', '2014-03-04 20:02:25'),
(10, 'Parts', '2014-03-04 20:02:34'),
(11, 'IT', '2014-03-04 20:03:01'),
(12, 'FMS', '2014-03-04 20:03:14'),
(14, 'CSR', '2014-03-04 20:03:25'),
(15, 'Payroll', '2014-03-04 20:03:39'),
(16, 'Administration', '2014-03-04 20:03:55'),
(17, 'Finance', '2014-03-04 20:04:08'),
(18, 'Vehicle Sales', '2014-03-04 20:06:15'),
(20, 'Yamaha', '2014-03-04 20:06:22'),
(21, 'Hino', '2014-03-04 20:06:32'),
(22, 'Branches', '2014-03-04 20:06:52'),
(23, 'Business Planning And Government', '2014-03-04 20:08:45'),
(24, 'ANZEN And Security', '2014-03-04 20:11:09'),
(25, 'TCAP', '2014-03-04 20:11:23'),
(26, 'Other', '2014-03-04 20:12:06');

-- --------------------------------------------------------

--
-- Table structure for table `sysusers`
--

CREATE TABLE IF NOT EXISTS `sysusers` (
  `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `fn` varchar(20) NOT NULL,
  `ln` varchar(20) NOT NULL,
  `dp` varchar(50) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `pro_pic` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `activated` int(10) NOT NULL,
  `activity` int(5) NOT NULL,
  `log` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nav` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `sysusers`
--

INSERT INTO `sysusers` (`u_id`, `email`, `fn`, `ln`, `dp`, `salt`, `pro_pic`, `pwd`, `activated`, `activity`, `log`, `nav`) VALUES
(25, 'admin@tk.com', 'sm', 'sm', 'Administration', '6599401375', 'sm@mail.com_avatar.png', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2014-12-11 09:41:07', '2014-06-08 15:19:40'),
(27, 'mdo@tk.com', 'md', 'officer', 'MDs Office', '1305995535', '7287202d0cf4f90695feb8c9422b25d8.png', '49e7afe4984f9f94060cf3ed0cbefd6d', 1, 1, '2015-07-29 11:29:50', '2014-12-10 20:56:08'),
(28, 'service@tk.com', 'Service', 'Service', 'Services', '1742412006', 'service@tk.com_avatar.png', 'aaabf0d39951f3e6c3e8a7911df524c2', 1, 0, '2015-05-18 11:37:04', '2015-02-28 08:27:40'),
(29, 'antony.sisule@toyotakenya.com', 'Antony', 'Sisule', 'Business Planning And Government', '3690329055', 'antony.sisule@toyotakenya.com_avatar.png', '3716114b005e9b4e533c7f3cd0f4f253', 1, 0, '2015-05-15 07:28:30', '2015-05-15 06:45:26'),
(31, 'hosea.yimbo@toyotakenya.com', 'Hosea', 'Yimbo', 'Business Planning And Government', '1993470890', 'hosea.yimbo@toyotakenya.com_avatar.png', 'd8d818a0ae1b39c2b75dec5ae2ba5ec2', 1, 1, '2015-05-15 07:42:19', '2015-05-15 07:40:26'),
(32, 'jeremiah.kanyi@toyotakenya.com', 'Jeremiah', 'Kanyi', 'Business Planning And Government', '8966108645', '2abcb917b684248844620d69f23c50c2.jpg', '33ca5a947f5598617186adb753ff2a21', 1, 1, '2015-05-15 11:32:14', '2015-05-15 07:44:59'),
(33, 'wanjiru.waiyaki@toyotakenya.com', 'Wanjiru', 'Waiyaki', 'Human Resources', '9973728285', 'wanjiru.waiyaki@toyotakenya.com_avatar.png', '2c869eba455f428ceac0c528c7514d16', 1, 0, '2015-05-15 08:47:37', '2015-05-15 08:46:18'),
(34, 'alex.munene@toyotakenya.com', 'Alex', 'Munene', 'Business Planning And Government', '8314121985', '530edf0ac28a4412a8167a7a4985b6c7.jpg', '534b44a19bf18d20b71ecc4eb77c572f', 1, 0, '2015-05-19 04:20:54', '2015-05-15 11:31:51'),
(35, 'justice.andiwo@toyotakenya.com', 'Justice', 'Andiwo', 'IT', '4897341635', 'justice.andiwo@toyotakenya.com_avatar.png', 'c53e479b03b3220d3d56da88c4cace20', 1, 0, '2015-05-18 07:57:27', '2015-05-18 07:51:34'),
(36, 'nehemiah.mutai@toyotakenya.com', 'Nehemiah', 'Mutai', 'Parts', '3049505865', 'nehemiah.mutai@toyotakenya.com_avatar.png', '17c7e2c490ef8537dff17e5949589682', 1, 0, '2015-05-18 08:33:24', '2015-05-18 08:32:52'),
(37, 'irene.mwihaki@toyotakenya.com', 'irene', 'mwihaki', 'Services', '2105036454', 'irene.mwihaki@toyotakenya.com_avatar.png', '3c24fe2808269610982031e445350db1', 0, 0, '2015-05-18 12:03:17', '2015-05-18 12:00:45'),
(38, 'moses.mbau@toyotakenya.com', 'Moses', 'Mbau', 'Business Planning And Government', '1661384689', 'moses.mbau@toyotakenya.com_avatar.png', '52bd43d37ed62eb4c226e31841bc03dc', 1, 0, '2015-05-20 04:29:06', '2015-05-20 04:29:06'),
(39, 'john.kalani@toyotakenya.com', 'John', 'Kalani', 'Business Planning And Government', '1093387897', 'john.kalani@toyotakenya.com_avatar.png', '527bd5b5d689e2c32ae974c6229ff785', 1, 1, '2015-05-20 05:20:14', '2015-05-20 04:30:50'),
(40, 'josephine.mwende@toyotakenya.com', 'Josephine', 'Mwende', 'Business Planning And Government', '7220573535', 'josephine.mwende@toyotakenya.com_avatar.png', 'f3c8e05a28650b1bf42179ba64b0d1b9', 1, 1, '2015-05-20 09:04:09', '2015-05-20 04:31:49'),
(41, 'martin.wamae@toyotakenya.com', 'Martin', 'Wamae', 'Business Planning And Government', '4583999175', 'martin.wamae@toyotakenya.com_avatar.png', '925d7518fc597af0e43f5606f9a51512', 1, 0, '2015-05-20 04:32:40', '2015-05-20 04:32:40'),
(42, 'uwe.lehmgrubler@toyotakenya.com', 'Uwe', 'Lehmgrubler', 'Services', '6160926805', 'uwe.lehmgrubler@toyotakenya.com_avatar.png', 'e6e0ec4af7c47316e06502f97bf9e26c', 1, 1, '2015-07-29 11:59:29', '2015-07-29 11:55:28'),
(43, 'damiana.mueni@toyotakenya.com', 'damiana', 'mueni', 'Customer Relations', '1965873779', 'damiana.mueni@toyotakenya.com_avatar.png', 'e9403a3edf96ef7be8102b61c385f621', 1, 1, '2015-07-29 13:14:54', '2015-07-29 13:14:08');

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE IF NOT EXISTS `upload` (
  `nt_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ct_id` int(10) NOT NULL,
  `u_id` int(10) NOT NULL,
  `file_desc` varchar(100) NOT NULL,
  `file` varchar(40) NOT NULL,
  `views` int(100) NOT NULL,
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`nt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `upload`
--

INSERT INTO `upload` (`nt_id`, `ct_id`, `u_id`, `file_desc`, `file`, `views`, `up_date`) VALUES
(62, 5, 42, 'Kenya', 'c40fae0eb7ecf2c804d4205caeaa515a.docx', 2, '2015-07-29 15:27:38'),
(63, 5, 42, 'Intro', '0c0c061fb9332d6236486c3f4deaf1f9.ppt', 3, '2015-07-29 15:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE IF NOT EXISTS `user_level` (
  `ur_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ur_desc` varchar(20) NOT NULL,
  PRIMARY KEY (`ur_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`ur_id`, `ur_desc`) VALUES
(1, 'System Admin'),
(2, 'Manager'),
(3, 'Publisher'),
(4, 'Writer');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
