<?php

/***

	@author: Simon Muraya :: My-IT-Provider Ltd.
	@created: 03.03.2014
	@modified: 13.08.2015

***/

//setcookie("tweek", "", strtotime( '-10 days' ), "http://10.40.1.225/dns", "", "", TRUE);

setcookie("tweek", "", strtotime( '-10 days' ), "http://localhost/dns", "", "", TRUE);

$tm_now = strtotime('now');

include 'notfy.php';

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Notice Board</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/dns.css?v=3.3_<?php echo $tm_now?>" />
        <link rel="stylesheet" type="text/css" href="css/dns_mobile.css?v=2.0_<?php echo $tm_now?>" media="screen and (max-height: 300px), screen and (orientation:portrait)" /><!--default 500px-->
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <script type="text/javascript" src="script/dns_gates.js?v=3.3_<?php echo $tm_now?>"></script>
    </head>
    <body>
        <div id="widget_scroll_container">
            <div class="widget_container full" data-num="0">
                <div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Administration" data-theme="orange" data-name="Administration">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget-logo.png');">
						<div class="ntf"><?php adm($con); ?></div>
                            <span>Administration</span>
                        </div>
                    </div>
                </div>
                <div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=ANZEN & Security" data-theme="orange" data-name="ANZEN & Security">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/anzen.png');">
						<div class="ntf"><?php ans($con); ?></div>
                            <span>ANZEN & Security</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Body Shop" data-theme="orange" data-name="Body Shop">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/body-shop.png'); background-size:contain;">
						<div class="ntf"><?php bs($con); ?></div>
                            <span>Body Shop</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Branches" data-theme="orange" data-name="Branches">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/branches.png'); background-size:contain; ">
						<div class="ntf"><?php bra($con); ?></div>
                            <span>Branches</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Business Planning & Goverment" data-theme="orange" data-name="Business Planning & Government">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget-gov.png'); background-size:contain; ">
						<div class="ntf"><?php bpg($con); ?></div>
                            <span>Business Planning & Government</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=CSR" data-theme="orange" data-name="CSR">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/cms.png');background-size:contain; ">
						<div class="ntf"><?php csr($con); ?></div>
                            <span>CSR</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Customer Relations" data-theme="orange" data-name="Customer Relations">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/cr.png'); background-size:contain;">
						<div class="ntf"><?php cus($con); ?></div>
                            <span>Customer Relations</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Finance" data-theme="orange" data-name="Finance">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/finance.png'); background-size:contain;">
						<div class="ntf"><?php fin($con); ?></div>
                            <span>Finance</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=FMS" data-theme="orange" data-name="FMS">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_timeline.png');">
						<div class="ntf"><?php fms($con); ?></div>
                            <span>FMS</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Hino" data-theme="orange" data-name="Hino">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/hino.png'); background-size:contain;">
						<div class="ntf"><?php hino($con); ?></div>
                            <span>Hino</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Human Resources" data-theme="orange" data-name="Human Resource">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/hr.png'); background-size:contain;">
						<div class="ntf"><?php hr($con); ?></div>
                            <span>Human Resources</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=IT" data-theme="orange" data-name="IT">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget-it.png'); background-size:contain;">
						<div class="ntf"><?php it($con); ?></div>
                            <span>IT</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=MDs Office" data-theme="orange" data-name="MDs Office">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/md.png'); background-size:contain; ">
						<div class="ntf"><?php mdo($con); ?></div>
                            <span>MDs Office</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Others" data-theme="orange" data-name="Others">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget-others.png'); background-size:contain;">
						<div class="ntf"><?php oth($con); ?></div>
                            <span>Others</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Parts" data-theme="orange" data-name="Parts">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget-parts.png'); background-size:contain;">
						<div class="ntf"><?php parts($con); ?></div>
                            <span>Parts</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Payroll" data-theme="orange" data-name="Payroll">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget-payroll.png'); background-size:contain;">
						<div class="ntf"><?php payr($con); ?></div>
                            <span>Payroll</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Services" data-theme="orange" data-name="Services">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/service.png'); background-size:contain;">
						<div class="ntf"><?php serv($con); ?></div>
                            <span>Services</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=TCAP" data-theme="orange" data-name="TCAP">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_royal_preloader.png');">
						<div class="ntf"><?php tcap($con); ?></div>
                            <span>TCAP</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Vehicle Sales" data-theme="orange" data-name="Vehicle Sales">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget-sales.png'); background-size:contain;">
						<div class="ntf"><?php vs($con); ?></div>
                            <span>Vehicle Sales</span>
                        </div>
                    </div>
                </div>
				<div class="widget widget4x2 widget_white animation unloaded" data-url="disnote.php?v=Yamaha" data-theme="orange" data-name="Yamaha">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/yamaha.png'); background-size:contain;">
						<div class="ntf"><?php yamaha($con); ?></div>
                            <span>Yamaha</span>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
        <div id="widget_preview">
            <div id="widget_sidebar">
                <div>
                    <div class="cancel"><span>Close</span></div>
                    <div class="refresh"><span>Refresh</span></div>
                    <div class="back"><span>Back</span></div>
                    <div class="next"><span>Next</span></div>
                </div>
            </div>
        </div>
		<?php //include 'mrd.php'; ?>
    </body>
</html>