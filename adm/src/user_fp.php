<?php
	include '/../cfg/cfg.php';
?>
<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button class="close" data-dismiss="modal" type="button"> x </button>
	<h4 class="modal-title">Please Fill In the Form Below To Reset Your Password</h4>
    </div>
	<div class="modal-body">
		<section class="tab-pane active" id="basic">
			<form action="?ac=send_req" class="form-horizontal" method="post" enctype="multipart/form-data" data-validate="parsley"> 
			<div class="form-group">
			<label class="col-sm-2">Email</label>
			<div class="col-sm-7">	
			<input type="rcemail" name="rcemail" class="form-control" required/>
			</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
				<button type="submit" class="btn btn-primary">Request For Password Reset</button>
                </div>
            </div>                  
			</form>               
		</section>
	</div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->	