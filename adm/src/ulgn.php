<?php
session_start();

include '/../cfg/cfg.php';
	
if(isset($_POST['xux']) && isset($_POST['xdn']) && isset($_POST['xpx'])){

	$em = $_POST['xux']; $dp = $_POST['xdn']; $p = $_POST['xpx'];
	
	if($em == ""){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty::Please Provide Your Email </strong></div>'; exit();
		
	}
	if($dp == ''){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty::Please Select Department </strong></div>'; exit();
		
	}
	if($p == ""){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty::Please Provide Password </strong></div>'; exit();
		
	}		
	$ecp = md5($p);
		
	$q = $con->query("SELECT * FROM sysusers WHERE email = '$em' AND dp = '$dp' AND activated = '1' AND pwd = '$ecp' LIMIT 1");
		
	$res = $q->fetch(PDO::FETCH_ASSOC);
		
	if($res){
	$slt = $res["salt"]; $adp = $res["dp"]; $pwd = $res["pwd"];
	$_SESSION['sxu'] = $slt; $_SESSION['dxu'] = $adp; $_SESSION['pxu'] = $pwd;
	
	$sqx = $con->exec("UPDATE sysusers SET activity = '1', log = now() WHERE email = '$em'");
			
	setcookie("csxu", $slt, strtotime( '+3 days' ), "/", "", "", TRUE);
	setcookie("cdxu", $adp, strtotime( '+3 days' ), "/", "", "", TRUE);
	setcookie("cpxu", $pwd, strtotime( '+3 days' ), "/", "", "", TRUE);			
			
	echo "<div class='alert alert-success'>
		<button type='button' class='close' data-dismiss='alert'><i class='icon-ok-sign'></i></button>
    <i class='icon-ban-circle'></i><strong> Successful </strong> Redirecting... </div><script>window.location.href='?ac=user';</script>";
			
	exit();
		
	}else{
		
	echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Wrong Credentials </strong> Try Again. </div>';
			
	exit();
		
	}
	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>   
    <meta charset="utf-8">   
    <title>Notice Board Admin | dnb</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/app.v1.css">
    <link rel="stylesheet" href="css/font.css" cache="false">
    <!--[if lt IE 9]>
	<script src="js/ie/respond.min.js" cache="false"></script>
	<script src="js/ie/html5.js" cache="false"></script>
	<script src="js/ie/fix.js" cache="false"></script>
	<![endif]-->
  </head>
  <body>
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp"> 
      <a class="nav-brand" href="index.html">Toyota Digital Notice Board</a>
		<div class="row m-n">
        <div class="col-md-4 col-md-offset-4 m-t-lg">
		<section class="panel">
            <header class="panel-heading text-center">Sign in</header>
            <form id="lgn" method="post" class="panel-body"> 
				<center><div id="login-info"></div></center>
				<div class="form-group"><input type="email" name="xux" placeholder="Email" class="form-control"></div>
				<div class="form-group">                   
                    <select name="xdn" class="form-control">
                        <option value="">Select Department</option>
<?php 
$dep = '';

$ty = $con->query("SELECT * FROM syscat");

foreach($ty as $r){

	$dep .= '<option value="'.$r['cat'].'" />'.$r['cat'].'';
			
}	
			
	echo $dep; 
	
?>	
                    </select>
				</div>
				<div class="form-group"><input type="password" name="xpx" id="inputPassword" placeholder="Password" class="form-control"></div>
				<a href="?ac=user_fp" data-toggle="ajaxModal" class="pull-right m-t-xs"><small>Forgot password?</small></a>
				<button type="submit" class="btn btn-info">Sign in</button>
            </form>
		</section>
		</div>
		</div>     
    </section>
    <!-- footer -->
	<footer id="footer">
	<div class="text-center padder clearfix">
	<p><small>*DNS<br>&copy; 2013-2015</small></p>
	</div>
	</footer>
	<!-- / footer -->	
    <script src="css/app.v2.js"></script>
	<script src="js/custom.js"></script>
    <!-- Bootstrap -->
    <!-- app -->
    </body>
</html>