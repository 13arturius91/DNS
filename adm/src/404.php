<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>404 || Page Not Found</title>
    <meta content=
    "app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"
    name="description">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name=
    "viewport">
    <link href="css/app.v1.css" rel="stylesheet">
    <link cache="false" href="css/font.css" rel="stylesheet">
    <!--[if lt IE 9]> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/html5.js" cache="false"></script> <script src="js/ie/fix.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section id="content">
        <div class="row m-n">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="text-center m-b-lg">
                    <h1 class="h text-white animated bounceInDown">404</h1>
                </div>
                <div class="list-group m-b-sm bg-white m-b-lg">
                    <a class="list-group-item" href="?ac=user">The Page Your Looking For Was Not Found</a> <a class="list-group-item" href="?ac=user">Click here to be redirected</a>
                </div>
            </div>
        </div>
    </section>
	<!-- footer -->
    <footer id="footer">
        <div class="text-center padder clearfix">
            <p><small>dns<br>&copy; 2015</small></p>
        </div>
    </footer>
	<!-- / footer -->
	<script src="css/app.v1.js"></script>
	<!-- Bootstrap -->
	<!-- app -->
</body>
</html>