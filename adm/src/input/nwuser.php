<?php

include '../../cfg/cfg.php';

if(isset($_POST['uex']) &&isset($_POST['fex']) &&isset($_POST['lex']) &&isset($_POST['dex']) &&isset($_POST['pwdex']) ){

	$em = $_POST['uex']; $fn = $_POST['fex']; $ln = $_POST['lex']; $dp = $_POST['dex']; $pwdex = $_POST['pwdex'];
	
	$salt = uniqid(mt_rand(), true);
	
	if($em == ""){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty :: Please Provide Email </strong></div>';
		
		exit();
		
	}
	
	if($fn == ""){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty :: Please Provide First Name </strong></div>';
		
		exit();
	
	}
	
	if($ln == ""){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty :: Please Provide Last Name </strong></div>';
		
		exit();
	
	}
	
	if($dp == "sd"){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty :: Please Select Department </strong></div>';
		
		exit();
	
	}
	
	if($pwdex == ""){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty :: Please Provide User Password </strong></div>';
		
		exit();
	
	}
	
	$encrypt = md5($pwdex);
	
	$uq = $con->query("SELECT * FROM sysusers WHERE email = '$em' LIMIT 1");
	
	$ur = $uq->fetch(PDO::FETCH_ASSOC);
	
	if($ur){
	
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> User With That Email Already Exist, Try Another Email </strong></div>';
		
		exit();
	
	}else{
	
		$atbm = $em.'_avatar.png';
	
		copy('../../images/def_avatar.png', '../pro_pic/'.$atbm);
	
		$c = $con->exec("INSERT INTO sysusers (email,fn,ln,dp,salt,pro_pic,pwd,activated,activity,log,nav) VALUES ('$em','$fn','$ln','$dp','$salt','$atbm','$encrypt','1','0',now(),now()) ");
		
		echo "<div class='alert alert-success'>
		<button type='button' class='close' data-dismiss='alert'><i class='icon-ok-sign'></i></button>
    <i class='icon-ban-circle'></i><strong> User Saved </strong></div>";
		
		exit();	
	
	}
}
?>