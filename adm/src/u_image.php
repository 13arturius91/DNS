<?php
	include '/../cfg/cfg.php';

	include 'inc/sess.php';
	
$allow = array('jpg', 'jpeg', 'png');

if(isset($_FILES['pfile']) ){

	$file = $_FILES['pfile']; 
	
	$extn = pathinfo($file['name'], PATHINFO_EXTENSION);
	
	if(!in_array(strtolower($extn), $allow)){
	
        echo '<script>alert("IMAGE FILES ONLY ALLOWED FOR UPLOAD"); window.location.href = "?ac=user";</script>';
		
        exit;
    }
	
	$fn = md5( time() ) . '.' . $extn;;

    if (file_exists("src/pro_pic/" . $fn )) {
	
		 echo '<script>alert("THAT IMAGE ALREADY EXISTS");</script>';
		
        exit;	
		
    }
	
	$qtp = $con->query("SELECT * FROM sysusers WHERE u_id = '$uid' LIMIT 1");

	$w = $qtp->fetch(PDO::FETCH_ASSOC); $ppc = $w['pro_pic']; 

	$pic_del = ('src/pro_pic/'.$ppc); 

	if(file_exists($pic_del)){
		unlink($pic_del);
	}else{
		echo '<script>alert("Image Not Deleted"); window.location.href = "?ac=adm"; </script>';
		exit();
	}
	
	if(move_uploaded_file($file['tmp_name'], 'src/pro_pic/' . $fn )){
        
        $s = $con->exec("UPDATE sysusers SET pro_pic = '$fn' WHERE u_id = '$uid'");	
		
		echo '<script>alert("SUCCESSFULL UPLOADED"); window.location.href = "?ac=user"; </script>';

		
    }

}
?>
<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button class="close" data-dismiss="modal" type="button">x</button>
	<h4 class="modal-title">Upload Your Profile Avatar</h4>
    </div>
	<div class="modal-body">
		<section class="tab-pane active" id="basic">
			<form action="?ac=u_image" class="form-horizontal" method="post" enctype="multipart/form-data" data-validate="parsley">                 
			<div class="form-group m-t-lg">
			<label class="col-sm-3 ">Profile Image</label>
			<div class="col-sm-9 media m-t-none">
                <div class="media-body">
				<input type="file" name="pfile" title="Change" class="btn btn-sm btn-info m-b-sm">
				</div>                  
            </div>                   
			</div>
			<div class="form-group">
				<div class="col-sm-4">
				<button type="submit" class="btn btn-primary">Upload</button>
                </div>
            </div>                  
			</form>               
		</section>
	</div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->	