	<!-- .vbox-->
	<section id="content">
		<section class="vbox">
		<header class="header bg-white b-b">
		<p><img src="images/widget-logo.png" style="height:35px; width:25px; padding-bottom:10px;"/>&nbsp;&nbsp;<text style="font-size:16px;">Welcome To Toyota Notice Board User Console</text></p>
		</header>
		<section class="scrollable wrapper">
			<div class="row">
				<div class="col-lg-12">
				<!-- .nav-justified-->
				<section class="panel">
					<header class="panel-heading bg-light">                   
                    <ul class="nav nav-tabs nav-justified">                      
                      <li class="active"><a href="#post_not" data-toggle="tab">Post Notices </a></li>
                      <li><a href="#wall" data-step="step5" data-toggle="tab">Wall </a></li>
					  <li><a href="#reports" data-toggle="tab">Reports </a></li>
					  <li><a href="#rep_to_admin" data-toggle="tab">Report To Admin </a></li>
                    </ul> 
					</header>
					<div class="panel-body">                   
                    <div class="tab-content">                     
                    <div class="tab-pane active" id="post_not" data-step="step1">
					<section class="panel">                 
                  <form action="?ac=user" class="form-horizontal" method="post" enctype="multipart/form-data" data-validate="parsley">
					<input type="text" rows="2" data-step="step2" name="pstn" placeholder="Write A Title For The Current Notice" class="form-control input-lg no-border" required>
					<input type="hidden" name="dep" value="<?php
						$d = $_COOKIE['cdxu']; 
						
						$cr = $con->query("SELECT * FROM syscat WHERE cat = '$d'");
						
						$cs = $cr->fetch(PDO::FETCH_ASSOC);
		
						if($cs){ $di = $cs["ct_id"]; echo $di; }
						?>" />
                    <footer class="panel-footer bg-light lter">                  
                    <button type="submit" data-step="step4" class="btn btn-info pull-right">POST</button>                    
                    <ul class="nav nav-pills">
					<div class="form-group m-t-lg">
						<div class="col-sm-9 media m-t-none">
						<div class="media-body" data-step="step3"><input type="file" name="pfile" title="Click Here To Upload Your Notice" class="btn btn-sm btn-info m-b-sm" required></div>                  
						</div>
					</div>
                    </ul>                    
                  </footer>
                  </form>               
					</section>
                    </div>                     
                    <div class="tab-pane" id="wall" data-step="00">
                    <section class="panel">                     
                      <header class="panel-heading">Posted Notices<label href="javascript:void(0);" onclick="wallIntro();" class="label bg-success m-l-xs" style="float: right;">help</label></header>     
                      <table class="table table-striped m-b-none text-sm">
                        <thead>  
                          <tr>
						  <th>Post Title</th>
						  <th data-step="01" >Views</th>
						  <th>Published Date</th>
						  <th data-step="02" width="70">Modify</th>
						  <th data-step="03" width="70">Delete</th>						  
						  </tr>
						</thead>
						<tbody>
<?php
$ln = '';

$qr = $con->query("SELECT * FROM upload WHERE ct_id  = '$di' ");

foreach($qr as $r){

	$ln .= '<tr data-name="'.$r["file_desc"].'">
	<td style="font-size:13.5px;"><i class="icon-file"></i> ' .$r["file_desc"]. '</td>
	<td><span class="badge bg-success">'.$r["views"].'<span></td>
	<td style="font-size:13px;">' .$r["up_date"]. '</td>
	<td><div class="btn-group"><a href="src/modify_notice.php?flnm='.$r["file"].'" data-toggle="ajaxModal"><i style="zoom:1.6;" class="icon-upload"></i></a>
	</div></td>
	<td><div class="btn-group"><a class="delpn" n_id='.$r["file"].' n_title="'.$r["file_desc"].'"><i style="zoom:1.6;" class="icon-trash"></i></a>
	</div></td>
	</tr>';
			
}			
		
	echo $ln; 

?>						   
                        </tbody>                       
                      </table>                    
                    </section>
                    </div>                     
                    <div class="tab-pane" id="reports">
						<section class="panel">
							<header class="panel-heading">Reports From Admin<label href="javascript:void(0);" onclick="repfrmadmIntro();" class="label bg-success m-l-xs" style="float: right;">help</label></header>
						<ul class="list-group alt" data-step="001">
<?php
$report = "";

$qyy = $con->query("SELECT * FROM report_admin INNER JOIN sysadmin ON report_admin.sys_id = sysadmin.sys_id AND dep = '$uedp' OR dep = 'superadmin' AND u_id ='$uid'");

foreach($qyy as $rr){

$tm_one = strtotime(''.$rr['rp_date'].'');
$tm_two = strtotime('now');
$dix = $tm_two - $tm_one;

$nnt = ago(time() - $dix);
	
	$report .='<li class="list-group-item">                            
                            <a class="thumb-sm pull-left m-r-sm">                              
                              <img src="src/pro_pic/'.$rr["pro_pic"].'" class="img-circle">                              
                            </a>                            
                            <a class="clear">                             
                              <small class="pull-right">
                                '.$nnt.'
                              </small> 								
                              <strong class="block">'.$rr["fn"].' '.$rr["ln"].' <small class="text-muted">&nbsp;&nbsp;Admin</small></strong>                              
                              <small>
                                '.$rr["rep_state"].'
                              </small>                             
                            </a>                           
                          </li>';
			
}			
		
	echo $report; 

?>						
						</ul>
						</section>
                    </div> 
					<!---->
					<div class="tab-pane" id="rep_to_admin">
						<header class="panel-heading" style="padding: 0px 2px !important; border-color: #FFFFFF !important;"> <label href="javascript:void(0);" onclick="reptoadmIntro();" class="label bg-success m-l-xs" style="float: right;">help</label></header>
					<section class="tab-pane active" id="basic">
					<article class="comment-item media" id="comment-form" data-step="0001">
                    
                    <a class="pull-left thumb-sm avatar"><img src="src/pro_pic/<?php echo $pp; ?>" class="img-circle"></a>
                    
                    <section class="media-body">
					<form action="?ac=rep_user" method="post" enctype="multipart/form-data" class="m-b-none">                       
                        <div class="input-group">						
						<input type="hidden" name="u_em" value="<?php echo $uemail; ?>" class="form-control">
<?php
$px = $con->query("SELECT * FROM sysadmin WHERE dep = '$uedp'");

$rw = $px->fetch(PDO::FETCH_ASSOC);
?>					
						<input type="hidden" name="adm_to" value="<?php echo $rw["sys_id" ]; ?>" class="form-control">
						<input type="text" class="form-control" name="urd" placeholder="Write A Report To Admin Here...." required>
						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit">POST</button>
						</span>
						</div>
					</form>
					</section>
					</article>
					</section>
					</div>
					<!---->	
                    </div>                    
                  </div>
				</section>
				<!---->	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<section data-step="step7" class="panel">
					<header class="panel-heading">User View Statistics [Bounce Rate]</header>
<?php
$ls = array();
$tm = array();

$ux = $con->query("SELECT * FROM upload WHERE u_id = '$uid'");

foreach($ux as $rx){
	$ls[] = "".$rx['views']."";
	$tm[] = "".$rx['up_date']."";
}

/* $old_pst = min($tm); 

$old_pst_time = strtotime($old_pst);

$cur_time = strtotime('now');

$diff_time = $cur_time - $old_pst_time;

$tyt = ago(time() - $diff_time); */

$max = max($ls); $min = min($ls); $tv = array_sum($ls);

$dif = $max - $min;

$perc = $dif/$tv * 100

?>				
						<div class="panel-body text-center">
							<h4><?php echo $tv; ?></h4><small class="text-muted block">Total Views</small>
							
							<div class="inline">
							<div class="easypiechart" data-line-width="16" data-loop="false" data-bar-color="#5BDE7B" data-percent="<?php echo round($perc); ?>" data-size="188">
								<span class="h2">0</span>%
								
								<div class="easypie-text">Currently</div>
								
							</div>
							</div>
						</div>
						
					<div class="panel-footer"><b>Below 50%</b>, People are interested to view most, if not all notices. <b>Above 50%</b>, People are interested to view particular notices rather than others.</div>
					</section>
				</div>
			</div>
		</section>
		</section>
	</section>
	<!-- e.vbox-->