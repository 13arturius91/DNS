	<aside class="bg-primary aside-sm nav-vertical" id="nav" style="width:20%;" >       
        <section class="vbox">         
          <header class="dker nav-bar">                  
            <a href="#" class="nav-brand" data-toggle="fullscreen">dnb</a>           
          </header>
          
          <footer class="footer bg-gradient hidden-xs">           
          </footer>         
          <section>           
            <!-- user -->           
            <div class="bg-success nav-user hidden-xs pos-rlt">             
              <div class="nav-avatar pos-rlt">               
                <a href="#" data-step="s6" class="thumb-sm avatar animated rollIn" data-toggle="dropdown">                 
                  <img src="src/pro_pic/<?php echo $pp; ?>" alt="" class="">                  
                  <span class="caret caret-white">
                  </span>                  
                </a>               
                <ul class="dropdown-menu m-t-sm animated fadeInLeft">                 
                  <span class="arrow top">
                  </span>                 
                  <li>                    
                    <a href="src/profile.php" data-toggle="ajaxModal">Edit Profile</a>              
                  </li>
                  <li>                    
                    <a href="src/image.php" data-toggle="ajaxModal">Upload Image</a>              
                  </li>
                  <li class="divider"></li>
                  <li><a href="?ac=adm&x=<?php echo $_COOKIE['apxu']; ?>">Logout</a></li>  
                </ul>
				<center><div class="visible-xs m-t m-b"><a href="#" class="h3"><?php echo $afn ." ". $aln; ?></a>
				<p><i class="icon-map-marker"></i>Nairobi, Kenya</p>
				</div></center> 
              </div>                        
            </div>           
            <!-- / user -->           
            <!-- nav -->          
            <nav class="nav-primary hidden-xs" style="background-color: #B6B6B6;">             
              <ul class="nav">              
                <li class="active"><a href="javascript:void(0);" <?php if($cdep == "superadmin"){ echo 'onclick="superIntro();"'; }else{ echo 'onclick="admIntro();"'; } ?>><i class="icon-eye-open"></i><span>DNS Admin Guide</span></a></li>                 
                </li>
              </ul> 
            </nav>
            <!-- / nav -->
            <!-- note -->  
            <div class="bg-danger wrapper hidden-vertical animated rollIn text-sm">            
              <a href="#" data-dismiss="alert" class="pull-right m-r-n-sm m-t-n-sm">
                <i class="icon-close icon-remove "></i>
              </a>
              Toyota DNS v1.3.1 
            </div>           
            <!-- / note -->           
          </section>         
        </section>      
      </aside>      
      <!-- /.aside -->