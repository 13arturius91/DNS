	</section>
    <script src="css/app.v2.js"></script>
	<script src="js/custom.js"></script>

	<script type="text/javascript" src="js/intro.js"></script>
	<script type="text/javascript">
      function userIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              { 
                intro: "<b style='color: #26252B;'>Welcome, you are a System User. You are responsible for posting and managing authorised notices for your department. Start DNS tutorial by clicking the next button.</b>"
              },
              { 
              	element: document.querySelector('[data-step="step1"]'),
                intro: "<b style='color: #26252B;'>Post notices in this section.</b>"
              },
              {
                element: document.querySelector('[data-step="step2"]'),
                intro: "<b style='color: #26252B;'>Write a Title for the post e.g('Vehicle Sales Report')</b>"
              },
              {
                element: document.querySelector('[data-step="step3"]'),
                intro: "<b style='color: #26252B;'>Click the 'Choose File' button to select any Microsoft Office file e.g(Word, Excel, Powerpoint, Access and Publisher).</b>"
                //position: 'right'
              },
              {
                element: document.querySelector('[data-step="step4"]'),
                intro: "<b style='color: #26252B;'>Click 'POST' button to upload the notice to the server.</b>",
                position: 'left'
              },
              {
                element: document.querySelector('[data-step="step5"]'),
                intro: "<b style='color: #26252B;'>Click the tab header to navigate to other tabs. Click the help button to get to know more about the section.</b>"
                //position: 'left'
              },
              {
                element: document.querySelector('[data-step="step6"]'),
                intro: "<b style='color: #26252B;'>Click The Image to allow you to 'Edit your Profile', 'Upload a Profile Picture' and 'Logout of the system'</b>",
                position: 'right'
              },
              {
                element: document.querySelector('[data-step="step7"]'),
                intro: "<b style='color: #26252B;'>User View Statistics, is a simple statistical summary of the percentage of views to a particular notice who navigate away from the notice after viewing only once.</b>",
                position: 'top'
              }
            ]
          });

          intro.start();
      }
      function wallIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              { 
                element: document.querySelector('[data-step="00"]'),
                intro: "<b style='color: #26252B;'>The Wall Section, displays every notice that you have uploaded to the system.</b>"
              },
              { 
                element: document.querySelector('[data-step="01"]'),
                intro: "<b style='color: #26252B;'>The Views Badge, displays the number of visits a notice has been viewed.</b>"
              },
              { 
                element: document.querySelector('[data-step="02"]'),
                intro: "<b style='color: #26252B;'>The Modify Button, is used when you want to update an already existing notice.</b>"
              },
              { 
                element: document.querySelector('[data-step="03"]'),
                intro: "<b style='color: #26252B;'>The Delete Button, is used when you want to delete an already existing notice.</b>",
                position: 'left'
              }
            ]
          });

          intro.start();
      }
      function repfrmadmIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              { 
                element: document.querySelector('[data-step="001"]'),
                intro: "<b style='color: #26252B;'>The Reports Section, displays the messages from the Super Admin or the Department Admin only.</b>"
              }
            ]
          });

          intro.start();
      }
      function reptoadmIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              { 
                element: document.querySelector('[data-step="0001"]'),
                intro: "<b style='color: #26252B;'>The Reports Section, displays the messages from the Super Admin or the Department Admin only.</b>"
              }
            ]
          });

          intro.start();
      }
      function superIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                intro: "<b style='color: #26252B;'>Welcome, you are a System Adminstrator. You are responsible for managing authorised notices, assigning departments, users in all departments and assigning new administrators for department. Start DNS tutorial by clicking the next button.</b>"
              },
              { 
                element: document.querySelector('[data-step="s1"]'),
                intro: "<b style='color: #26252B;'>The Notices Section, displays every notice that have been uploaded to the system by users from every department.</b>"
              },
              { 
                element: document.querySelector('[data-step="s2"]'),
                intro: "<b style='color: #26252B;'>The Views Badge, displays the number of visits a notice has been viewed by people.</b>",
                position: "left"
              },
              { 
                element: document.querySelector('[data-step="s3"]'),
                intro: "<b style='color: #26252B;'>The Modify Button, is used when you want to update an already existing notice.</b>",
                position: "left"
              },
              { 
                element: document.querySelector('[data-step="s4"]'),
                intro: "<b style='color: #26252B;'>The Delete Button, is used when you want to delete an already existing notice.</b>",
                position: "left"
              },
              {
                element: document.querySelector('[data-step="s5"]'),
                intro: "<b style='color: #26252B;'>Create new users for any department using their fullnames and emails. Remember, once you assign them their password send their credentials to their emails for login.</b>",
                position: "top"
              },
              { 
                element: document.querySelector('[data-step="s6"]'),
                intro: "<b style='color: #26252B;'>Click The Image to allow you to 'Edit your Profile', 'Upload a Profile Picture' and 'Logout of the system'.</b>",
                position: "right"
              },
              { 
                element: document.querySelector('[data-step="s7"]'),
                intro: "<b style='color: #26252B;'>Click the tab header to navigate to other tabs. Click the help button to get to know more about the section.</b>",
                position: "bottom"
              },
              { 
                element: document.querySelector('[data-step="s12"]'),
                intro: "<b style='color: #26252B;'>Create a new department, in this section.</b>",
                position: "bottom"
              },
              { 
                element: document.querySelector('[data-step="s13"]'),
                intro: "<b style='color: #26252B;'>Create new administrator for any department using their fullnames and emails. Remember, once you assign them their password send their credentials to their emails for login.</b>",
                position: "bottom"
              }
            ]
          });

          intro.start();
      }
      function admdepIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                element: document.querySelector('[data-step="s8"]'),
                intro: "<b style='color: #26252B;'>These are the current departments in Toyota. You can delete any of them by clicking this icon <i class='icon-pencil'></i> for that action.</b>"
              }
            ]
           });

           intro.start();
      }
      function admusrIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                element: document.querySelector('[data-step="s9"]'),
                intro: "<b style='color: #26252B;'>User section contains all users from every department. Click on their names to get more info about the user. From the pop-up can can delete the user from the system.</b>"
              }
            ]
           });

           intro.start();
      }
      function admrepusrIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                element: document.querySelector('[data-step="s10"]'),
                intro: "<b style='color: #26252B;'>This section contains all reports from every user. Their department names are next to their username.</b>"
              }
            ]
           });

           intro.start();
      }
      function admforpassIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                element: document.querySelector('[data-step="s11"]'),
                intro: "<b style='color: #26252B;'>In this section you will receive password reset request from other administartors of other department. Reset their password and send the new password to their emails.</b>"
              }
            ]
           });

           intro.start();
      }
      function usrforpassIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                element: document.querySelector('[data-step="a1"]'),
                intro: "<b style='color: #26252B;'>In this section you will receive password reset request from users in your department. Reset their password and send the new password to their emails.</b>"
              }
            ]
           });

           intro.start();
      }
      function admIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                intro: "<b style='color: #26252B;'>Welcome, you are a System Adminstrator in your department. You are responsible for managing authorised notices, assigning new users in your departments and handle password reset request form users. Start DNS tutorial by clicking the next button.</b>"
              },
              { 
                element: document.querySelector('[data-step="s1"]'),
                intro: "<b style='color: #26252B;'>The Notices Section, displays every notice that have been uploaded to the system by users from your department.</b>"
              },
              { 
                element: document.querySelector('[data-step="s2"]'),
                intro: "<b style='color: #26252B;'>The Views Badge, displays the number of visits a notice has been viewed by people.</b>",
                position: "left"
              },
              { 
                element: document.querySelector('[data-step="s3"]'),
                intro: "<b style='color: #26252B;'>The Modify Button, is used when you want to update an already existing notice.</b>",
                position: "left"
              },
              { 
                element: document.querySelector('[data-step="s4"]'),
                intro: "<b style='color: #26252B;'>The Delete Button, is used when you want to delete an already existing notice.</b>",
                position: "left"
              },
              {
                element: document.querySelector('[data-step="s5"]'),
                intro: "<b style='color: #26252B;'>Create new users for any department using their fullnames and emails. Remember, once you assign them their password send their credentials to their emails for login.</b>",
                position: "top"
              },
              { 
                element: document.querySelector('[data-step="s6"]'),
                intro: "<b style='color: #26252B;'>Click The Image to allow you to 'Edit your Profile', 'Upload a Profile Picture' and 'Logout of the system'.</b>",
                position: "right"
              },
              { 
                element: document.querySelector('[data-step="s7"]'),
                intro: "<b style='color: #26252B;'>Click the tab header to navigate to other tabs. Click the help button to get to know more about the section.</b>",
                position: "bottom"
              }
            ]
          });

          intro.start();
      }
      function adusrIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                element: document.querySelector('[data-step="a2"]'),
                intro: "<b style='color: #26252B;'>User section contains all users in your department. Click on their names to get more info about the user. From the pop-up can can delete the user from the system.</b>"
              }
            ]
           });

           intro.start();
      }
    </script>   
    <!-- Bootstrap -->
    
    <!-- Sparkline Chart -->
    
    <!-- App -->
</body>
</html>