<?php
@session_start();

/* auth admin cookies */
if(isset($_COOKIE["asxu"]) && isset($_COOKIE["apxu"])){
	$as = $_COOKIE["asxu"]; $ap = $_COOKIE["apxu"];
	
	$qa = $con->query("SELECT * FROM sysadmin WHERE salt = '$as' AND pwd = '$ap' LIMIT 1");
	
	$ars = $qa->fetch(PDO::FETCH_ASSOC);
	
		$sysid = $ars["sys_id"];
		$afn = $ars["fn"];
		$aln = $ars["ln"];
		$eq = $ars["em"];
		$cdep = $ars["dep"];
		$pp = $ars["pro_pic"];
		
		$_SESSION['id'] = $sysid;

}else{

		echo "<script>window.location.href='?ac=slogin';</script>";
		
		exit();

}

/*destroys admin session*/
if(isset($_REQUEST['x'])){

	$x = $_GET['x'];
	
	$xq = $con->query("SELECT * FROM sysadmin WHERE pwd = '$x' LIMIT 1");
	
	$rx = $xq->fetch(PDO::FETCH_ASSOC);
	
	if($rx){
	
		$_SESSION = array();
		
		setcookie('asxu', '', strtotime(' -20 days' ), '/');
		setcookie('apxu', '', strtotime(' -20 days' ), '/');
			
		session_destroy();
		
		header("location: ?ac=slogin");
		
		exit();
	
	}

}

function ago($time){

	date_default_timezone_set('Africa/Nairobi'); 
	
	$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	$lengths = array("60","60","24","7","4.35","12","10");
 
   $now = time();
 
   $difference = $now - $time;
   if ($difference > 0)
      $tense = 'ago';
   else
      $tense = 'in future';
 
   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }
 
   $difference = round($difference);
 
   if($difference != 1) {
       $periods[$j].= "s";
   }
	return "$difference $periods[$j] $tense ";
}
?>