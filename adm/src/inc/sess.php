<?php
@session_start();

if(isset($_COOKIE["csxu"]) && isset($_COOKIE["cdxu"]) && isset($_COOKIE["cpxu"])){
    $s = $_COOKIE['csxu']; //salt 
	$dpi = $_COOKIE['cdxu']; //department
	$p = $_COOKIE['cpxu'];	//password
	
	$q = $con->query("SELECT * FROM sysusers WHERE salt = '$s' AND dp = '$dpi' AND pwd = '$p' LIMIT 1");
		
	$res = $q->fetch(PDO::FETCH_ASSOC);
		
			$uid = $res["u_id"];
			$fn = $res["fn"];
			$ln = $res["ln"];
			$uemail = $res["email"];
			$uedp = $res["dp"];
			$pp = $res["pro_pic"];
				

			$_SESSION['id'] = $uid;	
			
}else{
	
		echo "<script>window.location.href='?ac=login';</script>";
		
		exit();
	
}

/*destorys user session*/
if(isset($_GET['y'])){
	
	$y = $_GET['y'];
	
	$qry = $con->query("SELECT * FROM sysusers WHERE pwd = '$y' LIMIT 1");
	
	$rs = $qry->fetch(PDO::FETCH_ASSOC);
	
	if($rs){
	
		$_SESSION = array();
		
		$xq = $con->exec("UPDATE sysusers SET activity = '0' WHERE pwd = '$y'");
		
		setcookie('csxu', '', strtotime(' -20 days' ), '/');
		setcookie('cdxu', '', strtotime(' -20 days' ), '/');
		setcookie('cpxu', '', strtotime(' -20 days' ), '/');
			
		session_destroy();
		
		header("location: ?ac=login");
		
		exit();
	
	}

}

function ago($time){

	date_default_timezone_set('Africa/Nairobi'); 
	
	$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	$lengths = array("60","60","24","7","4.35","12","10");
 
   $now = time();
 
   $difference = $now - $time;
   if ($difference > 0)
      $tense = 'ago';
   else
      $tense = 'in future';
 
   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }
 
   $difference = round($difference);
 
   if($difference != 1) {
       $periods[$j].= "s";
   }
	return "$difference $periods[$j] $tense ";
}
?>