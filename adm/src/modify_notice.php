<?php
	if(isset($_REQUEST['flnm'])){
		
		$flnm = $_REQUEST['flnm'];
		
	}
?>
<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button class="close" data-dismiss="modal" type="button">x</button>
	<h4 class="modal-title">Update This File In The System</h4>
    </div>
	<div class="modal-body">
		<section class="tab-pane active" id="basic">
			<form action="?ac=update_notice" class="form-horizontal" method="post" enctype="multipart/form-data" data-validate="parsley">                 
			<div class="form-group m-t-lg">
			<label class="col-sm-3 ">Upload File</label>
			<div class="col-sm-9 media m-t-none">
                <div class="media-body">
				<input type="file" name="newfile" title="Change" class="btn btn-sm btn-info m-b-sm">
				<input type="hidden" name="re_file_name" value="<?php echo $flnm; ?>"title="Change" class="btn btn-sm btn-info m-b-sm">
				</div>                  
            </div>                   
			</div>
			<div class="form-group">
				<div class="col-sm-4">
				<button type="submit" class="btn btn-primary">Upload</button>
                </div>
            </div>                  
			</form>               
		</section>
	</div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->