<?php

include '../../inc/cnf.php';

@session_start();

$allow = array('doc' ,'docx' ,'ppt' ,'pptx' ,'pdf');

if(isset($_POST['desx']) && isset($_POST['dep']) && isset($_FILES['upl']) && $_FILES['upl']['error'] == 0  ){

	$decr = $_POST['desx'];

	$dep = $_POST['dep'];
	
	$file = $_FILES['upl'];
	
	$efile = $_FILES['upl']['error'];
	
	if($efile){
	
        echo '{"message" : "File Error"}';
		
        exit;
    }

	if ($file == ""){
	
        echo '{"message" : "Empty::Please Insert Image"}';
		
        exit;
    }
	
	if($dep == ""){
	
		echo '{ "message" : "Empty::Please Select Department"}';
		
		exit();
	
	}
	
	if($decr == ""){
	
		echo '{ "message" : "Empty::Please Right a Short Description"}';
	
	}
	
	$extn = pathinfo($file['name'], PATHINFO_EXTENSION);
	
	if(!in_array(strtolower($extn), $allow)){
	
        echo '{ "message" : "Docs Allowed with Extension of .doc ,.docx ,.ppt ,.pptx ,.pdf"}';
		
        exit;
    }
	
	$fn = md5( time() ) . '.' . $extn;;

    if (file_exists("../../../upld/" . $fn )) {
	
        echo '{ "message" : " ' . $file["name"] . ' Already Exists. ' . ' " }';
		
        exit;	
    }
	
	if(move_uploaded_file($file['tmp_name'], '../../../upld/'.$fn )) {
        
        $s = $con->exec("INSERT INTO upload (ct_id, u_id, file_desc, file, up_date) VALUES ('$dep','". trim($_SESSION['id']) ."', '$decr', '$fn', now())");
		
		echo '{ "message" : "Successful::Upload Complete" }';
		
        exit;
    }

}
?>