<?php
	include '/../cfg/cfg.php';

	include 'inc/ad_sess.php';

if(isset($_GET['gtu'])){

	$gtu = $_GET['gtu']; 
	
    $s = $con->query("SELECT * FROM sysusers WHERE salt = '$gtu' LIMIT 1");	
	
	$qq = $s->fetch(PDO::FETCH_ASSOC);
	
	$gid = $qq["u_id"]; $gem = $qq["email"]; $gfn = $qq["fn"]; $gln = $qq["ln"]; $gdp = $qq["dp"]; $gimg = $qq["pro_pic"];
		

}
?>
<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button class="close" data-dismiss="modal" type="button">x</button>
	<h4 class="modal-title">User Info</h4>
    </div>
	<div class="modal-body">
		<section class="tab-pane active" id="basic">           
                  <section class="scrollable">                   
                    <div class="wrapper">                     
                      <div class="clearfix m-b">                       
                        <a href="#" class="pull-left thumb m-r">                         
                          <img src="src/pro_pic/<?php echo $gimg; ?>" class="img-circle">                        
                        </a>                        
                        <div class="clear">                         
                          <div class="h3 m-t-xs m-b-xs">
                            <?php echo $gfn . ' ' .$gln; ?>
                          </div>                         
                          <small class="text-muted">
                            <i class="icon-map-marker">
                            </i>
                            Nairobi,Kenya
                          </small>                         
                        </div>                       
                      </div>                     
                      <div class="panel wrapper">                        
                        <div class="row">                        
                          <div class="col-xs-9">                          
                            <a href="#">                            
                              <span class="m-b-xs h4 block">
                                <?php echo $gem; ?>
                              </span>                              
                              <small class="text-muted">
                                email
                              </small>                             
                            </a>                           
                          </div>                         
                          <div class="col-xs-9">                           
                            <a href="#">                             
                              <span class="m-b-xs h4 block">
                                <?php echo $gdp; ?>
                              </span>                              
                              <small class="text-muted">
                                department
                              </small>                             
                            </a>                           
                          </div>                                                 
                        </div>                        
                      </div>                      
                      <div class="btn-group btn-group-justified m-b">                                             
                        <a href="?ac=del_user&udel=<?php echo $gid; ?>" class="btn btn-info btn-rounded">
						<i class="icon-ban-circle"></i>
                          Delete This User 
                        </a>                     
                      </div>                                        
                    </div>                   
                  </section>                      
		</section>
	<script src="css/app.v1.js"></script>
	<script src="js/custom.js"></script>
	</div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->