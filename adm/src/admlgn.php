<?php
session_start();

include '/../cfg/cfg.php';
	
if(isset($_POST['xaux']) && isset($_POST['xaix']) && isset($_POST['xapx'])){
	$u = $_POST['xaux']; $ai = trim($_POST['xaix']); $p = trim($_POST['xapx']);
	if($u == ""){
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty::Provide Your Email </strong></div>'; exit();
	}
	if($ai == ''){
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty::Provide AI Number </strong></div>'; exit();
	}
	if($p == ""){
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Empty::Please Provide Password </strong></div>'; exit();
	}
	
	$ecp = md5($p);
	
	$q = $con->query("SELECT * FROM sysadmin WHERE em = '$u' AND ai = '$ai' AND pwd = '$ecp'");
	
	$res = $q->fetch(PDO::FETCH_ASSOC);
		
	if($res){
	$slt = $res["salt"]; $pwd = $res["pwd"];
	$_SESSION['sxu'] = $slt; $_SESSION['pxu'] = $pwd;
	
	setcookie("asxu", $slt, strtotime( '+1 days' ), "/", "", "", TRUE);
	setcookie("apxu", $pwd, strtotime( '+1 days' ), "/", "", "", TRUE);			
			
		echo "<div class='alert alert-success'>
		<button type='button' class='close' data-dismiss='alert'><i class='icon-remove'></i></button>
		<i class='icon-ok-sign'></i><strong> Successful </strong> Redirecting... </div> <script>window.location.href='?ac=adm';</script>";
			
		exit();
		
	}else{
		
		echo '<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <i class="icon-ban-circle"></i><strong> Wrong Credentials </strong> Try Again. </div>';
			
		exit();
		
	}
	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>   
    <meta charset="utf-8">   
    <title>Notice Board Admin | dnb</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/app.v1.css">
    <link rel="stylesheet" href="css/font.css" cache="false">
    <!--[if lt IE 9]>
	<script src="js/ie/respond.min.js" cache="false"></script>
	<script src="js/ie/html5.js" cache="false"></script>
	<script src="js/ie/fix.js" cache="false"></script>
	<![endif]-->
  </head>
  <body>
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp"> 
      <a class="nav-brand" href="index.html">Toyota Digital Notice Board</a>
		<div class="row m-n">
        <div class="col-md-4 col-md-offset-4 m-t-lg">
		<section class="panel">
            <header class="panel-heading text-center">Sign in</header>
            <form id="act" method="post" class="panel-body"> 
				<center><div id="info"></div></center>
				<div class="form-group"><input type="email" name="xaux" placeholder="Email" class="form-control"></div>
				<div class="form-group"><input type="text" name="xaix" placeholder="AI Number" class="form-control"></div>
				<div class="form-group"><input type="password" name="xapx" id="inputPassword" placeholder="Password" class="form-control"></div>
				<a href="?ac=adm_fp" data-toggle="ajaxModal" class="pull-right m-t-xs"><small>Forgot password?</small></a>
				<button type="submit" class="btn btn-info">Sign in</button>
            </form>
		</section>
		</div>
		</div>     
    </section>
    <!-- footer -->
	<footer id="footer">
	<div class="text-center padder clearfix">
	<p><small>*DNS<br>&copy; 2013-2015</small></p>
	</div>
	</footer>
	<!-- / footer -->	
    <script src="css/app.v2.js"></script>
	<script src="js/custom.js"></script>
    <!-- Bootstrap -->
    <!-- app -->
    </body>
</html>