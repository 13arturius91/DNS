	<!-- .vbox-->
	<section id="content">
		<section class="vbox">
		<header class="header bg-white b-b">
		<p><img src="images/widget-logo.png" style="height:35px; width:25px; padding-bottom:10px;"/>&nbsp;&nbsp;<text style="font-size:16px;">Welcome To Toyota Notice Board Admin Console</text></p>
		</header>
		<section class="scrollable wrapper">
			<div class="row">
				<div class="col-lg-10">
				<!-- .nav-justified-->
				<section class="panel">
					<header class="panel-heading bg-light">                   
                    <ul class="nav nav-tabs nav-justified">                      
                    <li class="active"><a href="#notices" data-toggle="tab">Notices</a></li>
                    <?php if($cdep == 'superadmin'){ echo '<li><a href="#departments" data-toggle="tab">Departments</a></li>'; } else { echo ''; }?> 					
					<li><a href="#users" data-step="s7" data-toggle="tab">Users</a></li>
					<li><a href="#rep_users" data-toggle="tab">Reports From Users</a></li>					
					<?php if($cdep == 'superadmin'){ echo ''; } else { echo '<li><a href="#fog_pass" data-toggle="tab">User Forgot Password</a></li>'; }?> 
					<?php if($cdep == 'superadmin'){ echo '<li><a href="#fog_pass_adm" data-toggle="tab">Admin Forgot Password</a></li>'; } else { echo ''; }?>
                    </ul> 
					</header>
					<div class="panel-body">                   
                    <div class="tab-content">                     
                    <div class="tab-pane active" data-step="s1" id="notices">
					<section class="panel" style="overflow-y:scroll; height:300px;">                     
                      <header class="panel-heading">                        
                        Posted Notices 
                      </header>     
                      <table class="table table-striped m-b-none text-sm">
                        <thead>  
                          <tr>
						  <th>File Title</th>
						  <th data-step="s2">Views</th>
						  <th>Published Date</th>
						  <?php if($cdep == 'superadmin'){ echo "<th>Departmet</th>"; }else{ ""; } ?>
						  <th data-step="s3" width="70">Modify</th>
						  <th data-step="s4" width="70">Delete</th>
						  </tr>
						</thead>
						<tbody>
<?php
if($cdep == 'superadmin'){
$ln = '';
$qr = $con->query("SELECT * FROM upload INNER JOIN syscat ON syscat.ct_id = upload.ct_id");
foreach($qr as $r){
			
	$ln .='<tr data-name="'.$r["file_desc"].'">
	<td style="font-size:13.5px;"><i class="icon-file"></i> ' .$r["file_desc"]. '</td>
	<td><span class="badge bg-success">'.$r["views"].'<span></td>
	<td style="font-size:13px;" >' .$r["up_date"]. '</td>
	<td style="font-size:13px;" >' .$r["cat"]. '</td>	
	<td><div class="btn-group"><a href="src/modify_notice.php?flnm='.$r["file"].'" data-toggle="ajaxModal"><i style="zoom:1.6;" class="icon-upload"></i></a>
	</div></td>
	<td><div class="btn-group"><a class="delpnadm" not_id='.$r["file"].' not_title="'.$r["file_desc"].'"><i style="zoom:1.6;" class="icon-trash"></i></a>
	</div></td>
	</tr>';		
			
}
}else{
$ln = '';
$qr = $con->query("SELECT * FROM upload INNER JOIN syscat ON syscat.ct_id = upload.ct_id AND cat = '$cdep' ");
foreach($qr as $r){
			
	$ln .='<tr id="ln'.$r["ct_id"].'">
	<td style="font-size:13.5px;"><i class="icon-file"></i> ' .$r["file_desc"]. '</td>
	<td><span class="badge bg-success">'.$r["views"].'<span></td>
	<td>' .$r["up_date"]. '</td>
	<td><div class="btn-group"><a class="modpn" not_id='.$r["file"].' n_title="'.$r["file_desc"].'"><i style="zoom:1.6;" class="icon-upload"></i></a>
	</div></td>
	<td><div class="btn-group"><a class="delpnadm" not_id='.$r["file"].' not_title="'.$r["file_desc"].'"><i style="zoom:1.6;" class="icon-trash"></i></a>
	</div></td>
	</tr>';		
			
}
}					
	echo $ln; 
?>						   
                        </tbody>                       
                      </table>                    
                    </section>
                    </div>                     
                    <div class="tab-pane" data-step="s8" id="departments">
                    <section class="panel" style="overflow-y:scroll; height:300px;">                     
                      <header class="panel-heading">Listed Departments <label href="javascript:void(0);" onclick="admdepIntro();" class="label bg-success m-l-xs" style="float: right;">help</label></header>     
                      <table class="table table-striped m-b-none text-sm">
                        <thead>  
                          <tr>
						  <th>Departments</th>
						  <th>Date Made</th>
						  <th width="70">Delete</th>
						  </tr>
						</thead>
						<tbody>
<?php
/*departments*/
$cs = '';

$uq = $con->query("SELECT * FROM syscat");

foreach($uq as $rw){

	$cs .='<tr id="ct' .$rw["ct_id"]. '">
			<td>'.$rw["cat"].'</td>
            <td>'.$rw["dt"].'</td>
            <td class="text-right"><div class="btn-group"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="icon-pencil"></i></a>
				<ul class="dropdown-menu pull-right">
			<li><a class="delct" ct_id='.$rw["ct_id"].' ct_name="'.$rw["cat"].'">Delete</a></li>
				</ul>
			</div></td>
			</tr>';		
			
}					
	echo $cs; 

?>   
                        </tbody>                       
                      </table>                    
                    </section>
                    </div>                     
                    <div class="tab-pane" <?php if($cdep == "superadmin"){ echo 'data-step="s9"'; }else{ echo 'data-step="a2"'; } ?> id="users">
                    	<header class="panel-heading" style="padding: 0px 0px !important; border-color: #FFFFFF !important;"> <label href="javascript:void(0);" <?php if($cdep == "superadmin"){ echo 'onclick="admusrIntro();"'; }else{ echo 'onclick="adusrIntro();"'; } ?>  class="label bg-success m-l-xs" style="float: right;">help</label></header>					
						<section class="panel" style="overflow-y:scroll; height:300px;">
						<ul class="list-group alt">
<?php
/*users*/
if($cdep == 'superadmin'){
$us = '';
$u = $con->query("SELECT * FROM sysusers");
foreach($u as $r){	
$tm_one = strtotime(''.$r['log'].'');
$tm_two = strtotime('now');
$diff = $tm_two - $tm_one;

if($diff){ $tt = ago(time() - $diff); }else{ $tt = ''; }
if( $r["activity"] == "1" ){ $a = "success"; }
if( $r["activity"] == "0" ){ $a = "muted"; }	
			
	$us .= '<li class="list-group-item">
			<div class="media">
					<span class="pull-left thumb-sm">
						<img src="src/pro_pic/'.$r["pro_pic"].'" alt="'.$r["fn"].' '.$r["ln"].'" class="img-circle">
					</span>
				<div class="pull-right text-'.$a.' m-t-sm"><i class="icon-circle"></i></div>
			<div class="media-body">
			<div><a href="src/users_profile.php?gtu='.$r["salt"].'" data-toggle="ajaxModal">'.$r["fn"].' '.$r["ln"].'</a></div>
			<small class="text-muted">'.$tt.'</small>
			</div>
				</div>
			</li>';			
}			
		 
}else{

$us = '';
$u = $con->query("SELECT * FROM sysusers WHERE dp = '$cdep'");
foreach($u as $r){	
$tm_one = strtotime(''.$r['log'].'');
$tm_two = strtotime('now');
$diff = $tm_two - $tm_one;

if($diff){ $tt = ago(time() - $diff); }else{ $tt = ''; }
if( $r["activity"] == "1" ){ $a = "success"; }
if( $r["activity"] == "0" ){ $a = "muted"; }	
			
	$us .= '<li class="list-group-item">
			<div class="media">
					<span class="pull-left thumb-sm">
						<img src="src/pro_pic/'.$r["pro_pic"].'" alt="'.$r["fn"].' '.$r["ln"].'" class="img-circle">
					</span>
				<div class="pull-right text-'.$a.' m-t-sm"><i class="icon-circle"></i></div>
			<div class="media-body">
			<div><a href="src/users_profile.php?gtu='.$r["salt"].'" data-toggle="ajaxModal">'.$r["fn"].' '.$r["ln"].'</a></div>
			<small class="text-muted">'.$tt.'</small>
			</div>
				</div>
			</li>';	
	
			
}

}			
		
	echo $us; 	

?>
						</ul>						
						</section>
                    </div>                      
                      <div class="tab-pane" data-step="s10" id="rep_users">
                      	<label href="javascript:void(0);" onclick="admrepusrIntro();" class="label bg-success m-l-xs" style="float: right;">help</label>
						<section class="panel" style="overflow-y:scroll; height:300px;">
						<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-lg no-border">
						<!-- <span class="label label-info">Reply</span> <span class="label label-info">Hide Report</span> -->
<?php
if($cdep == 'superadmin'){
$report = "";
$qyy = $con->query("SELECT * FROM report_user INNER JOIN sysusers ON report_user.u_email = sysusers.email");
foreach($qyy as $rr){
$tm_one = strtotime(''.$rr['rp_date'].'');
$tm_two = strtotime('now');
$dix = $tm_two - $tm_one;

$nnt = ago(time() - $dix);
	
	$report .='<li class="list-group-item">                            
                <a class="thumb-sm pull-left m-r-sm">                              
                    <img src="src/pro_pic/'.$rr["pro_pic"].'" class="img-circle">                              
                            </a>
                              <small class="pull-right"> '.$nnt.' </small>
                              <a href="src/users_profile.php?gtu='.$rr["salt"].'" data-toggle="ajaxModal" class="clear"><strong class="block">'.$rr["fn"].' '.$rr["ln"].' <small class="text-muted">&nbsp;&nbsp;'.$rr["dp"].'</small>
                              </strong></a>                             
                              <small>'.$rr["rep_state"].'</small>                             
							</a>
                          </li>';
			
}
}else{
$report = "";
$sqly = $con->query("SELECT * FROM report_user INNER JOIN sysusers ON report_user.u_email = sysusers.email AND sys_id = '$sysid'");
foreach($sqly as $rrx){
$tm_on = strtotime(''.$rrx['rp_date'].'');
$tm_tw = strtotime('now');
$difx = $tm_tw - $tm_on;

$nnnt = ago(time() - $difx);
	
	$report .='<li class="list-group-item">                            
                <a class="thumb-sm pull-left m-r-sm">                              
                    <img src="src/pro_pic/'.$rrx["pro_pic"].'" class="img-circle">                              
                            </a>                            
                            <a href="src/users_profile.php?gtu='.$rrx["salt"].'" data-toggle="ajaxModal" class="clear">                             
                              <small class="pull-right">
                                '.$nnnt.'
                              </small>
                              <strong class="block">'.$rrx["fn"].' '.$rrx["ln"].' <small class="text-muted">&nbsp;&nbsp;'.$rrx["dp"].'</small>
                              </strong>                             
                              <small>
                                '.$rrx["rep_state"].'
                              </small>                             
                            </a>                           
                          </li>';
			
}
}			
		
	echo $report; 

?>												                         
                          
						</ul>
						</section>
                    </div>
					<div class="tab-pane" data-step="a1" id="fog_pass">
					<label href="javascript:void(0);" onclick="usrforpassIntro();" class="label bg-success m-l-xs" style="float: right;">help</label>
					<section class="tab-pane active" id="basic">               
					<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-lg">
<?php
$fprep = "";

//$qfp = $con->query("SELECT sysusers.dp FROM fog_pass, sysusers WHERE fog_pass.u_email = sysusers.email AND sysusers.dp = '$cdep' ");
$qfp = $con->query("SELECT * FROM fog_pass INNER JOIN sysusers ON fog_pass.u_email = sysusers.email AND sysusers.dp = '$cdep' ");

foreach($qfp as $rp){

$te = strtotime(''.$rp['req_dt'].'');
$tet = strtotime('now');
$dt = $tet - $te;

$adt = ago(time() - $dt);
	
	$fprep .='<li class="list-group-item"><a class="thumb-sm pull-left m-r-sm"><img src="src/pro_pic/'.$rp["pro_pic"].'" class="img-circle"></a><a href="src/pass_reset.php?pwdr='.$rp["salt"].'" data-toggle="ajaxModal" class="clear">
	<small class="pull-right">'.$adt.'</small><strong class="block">'.$rp["fn"].' '.$rp["ln"].' <small class="text-muted">&nbsp;&nbsp;'.$rp["dp"].'</small></strong><small>Password Reset Request</small></a></li>';
			
}

echo (!$fprep == "") ? $fprep : '<div class="alert alert-info"> <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button> <i class="fa fa-info-sign"></i><strong>No User Has Requested For A Password Reset Yet !</strong></div>'; 

?>												                         
                    </ul>
					</section>
                    </div>
					<div class="tab-pane" data-step="s11" id="fog_pass_adm">
					<label href="javascript:void(0);" onclick="admforpassIntro();" class="label bg-success m-l-xs" style="float: right;">help</label>
					<section class="tab-pane active" id="basic">               
					<ul class="list-group no-radius m-b-none m-t-n-xxs list-group-lg">
<?php
$fogp = "";

$qp = $con->query("SELECT * FROM fog_pass_ad INNER JOIN sysadmin ON fog_pass_ad.ad_email = sysadmin.em");

foreach($qp as $rw){

$tm_o = strtotime(''.$rw['req_dt'].'');
$tm_t = strtotime('now');
$fpix = $tm_t - $tm_o;

$fpdt = ago(time() - $fpix);
	
	$fogp .='<li class="list-group-item"><a class="thumb-sm pull-left m-r-sm"><img src="src/pro_pic/'.$rw["pro_pic"].'" class="img-circle"></a><a href="src/pass_reset.php?admpwdr='.$rw["salt"].'" data-toggle="ajaxModal" class="clear">
	<small class="pull-right">'.$fpdt.'</small><strong class="block">'.$rw["fn"].' '.$rw["ln"].' <small class="text-muted">&nbsp;&nbsp;'.$rw["dep"].'</small></strong><small>Password Reset Request</small></a></li>';
			
}	
		
echo (!$fogp == "") ? $fogp : '<div class="alert alert-info"> <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button> <i class="fa fa-info-sign"></i><strong>No Admin Has Requested For A Password Reset Yet !</strong></div>';
?>												                         
                    </ul>
					</section>
					</div>					
                    </div>                    
                  </div>
				</section>
				<!---->	
				<!--settings-->
				<section class="panel">
					<header class="panel-heading bg-light">                   
                    <ul class="nav nav-tabs nav-justified"> 
						<li class="active"><a href="#add_users" data-toggle="tab">Add New User</a></li>
					  <?php if($cdep == 'superadmin'){ echo '<li><a href="#add_dep" data-step="s12" data-toggle="tab">Add New Department</a></li>'; } else { echo ''; }?>					  
                      <?php if($cdep == 'superadmin'){ echo '<li><a href="#add_admins" data-step="s13" data-toggle="tab">Add New Admin</a></li>'; } else { echo ''; }?>  
                    </ul> 
					</header>
					<div class="panel-body">                   
                    <div class="tab-content">                                                               
                    <div class="tab-pane active" data-step="s5" id="add_users">
					<section class="tab-pane active" id="basic">               
					<form id="nwus" class="form-horizontal" method="post" data-validate="parsley">                 
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="email" name="uex" placeholder="User Email" class="form-control"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="fex" placeholder="User First Name" class="form-control"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="lex" placeholder="User Last Name" class="form-control"></div>
					</div>
					<div class="form-group">                                     
                    <div class="col-sm-4 col-sm-offset-3">                      
						<select name="dex" class="form-control">                       
                        <option value="sd">Select Department</option>
<?php 
if($cdep == 'superadmin'){

$dep = '';

$ty = $con->query("SELECT * FROM syscat");

foreach($ty as $r){ $dep .= '<option value="'.$r['cat'].'" />'.$r['cat'].''; }	

}else{ $dep = '<option value="'.$cdep.'" />'.$cdep.''; }	
		
	echo $dep; 
	
?>						
						</select>                      
                    </div>                   
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="pwdex" placeholder="User Password" class="form-control"></div>
					</div>
					<center><div id="user-info"></div></center>
					<div class="form-group">
					<div class="col-sm-4 col-sm-offset-3">
						<button type="reset" class="btn btn-white">Cancel</button>
						<button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </div>                  
					</form>               
					</section>		
                    </div>
					<div class="tab-pane" id="add_dep">
					<section class="tab-pane active" id="basic">               
					<form id="nwdep" class="form-horizontal" method="post" data-validate="parsley">                 
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="cex" placeholder="Department Name" class="form-control"></div>
					</div>
					<center><div id="dep-info"></div></center>
					<div class="form-group">
					<div class="col-sm-4 col-sm-offset-3">
						<button type="reset" class="btn btn-white">Cancel</button>
						<button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </div>                  
					</form>               
					</section>
                    </div>
					<div class="tab-pane" id="add_admins">
					<section class="tab-pane active" id="basic">               
					<form id="admus" class="form-horizontal" method="post" data-validate="parsley">                 
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="email" name="aduex" placeholder="Admin Email" class="form-control"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="adfex" placeholder="Admin First Name" class="form-control"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="adlex" placeholder="Admin Last Name" class="form-control"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="adaiex" placeholder="Admin AI Number" class="form-control"></div>
					</div>
					<div class="form-group">
					<div class="col-sm-4 col-sm-offset-3">                      
						<select name="adex" class="form-control">                       
                        <option value="sd">Select Department</option>
						<option value="superadmin">Super Admin</option>
<?php 
$dep = '';
$ty = $con->query("SELECT * FROM syscat");
foreach($ty as $r){
	$dep .= '<option value="'.$r['cat'].'" />'.$r['cat'].'';			
}				
	echo $dep; 	
?>						
						</select>                      
                    </div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-3"><input type="text" name="adpdex" placeholder="User Password" class="form-control"></div>
					</div>
					<center><div id="adm-info"></div></center>
					<div class="form-group">
					<div class="col-sm-4 col-sm-offset-3">
						<button type="reset" class="btn btn-white">Cancel</button>
						<button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </div>                  
					</form>               
					</section>
					</div>	
                    </div>                    
                  </div>
				</section>
				</div>
			</div>
		</section>
		</section>
	</section>
	<!-- e.vbox-->