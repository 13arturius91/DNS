<?php
	include '/../cfg/cfg.php';

	include 'inc/sess.php';

	include 'inc/hd.php';
	
	include 'inc/u_sd.php';
	
/* add new notice */	

$allow = array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx');

if(isset($_POST['pstn']) && isset($_FILES['pfile']) ){

	$xpnt = $_POST['pstn'];
	$xdep = $_POST['dep'];
	$xfnt = $_FILES['pfile']; 
	
	$extn = pathinfo($xfnt['name'], PATHINFO_EXTENSION);
	
	if(!in_array(strtolower($extn), $allow)){
	
        echo '<script>alert("ONLY DOCUMENTS ALLOWED FOR UPLOAD"); window.location.href = "?ac=user";</script>';
		
        exit;
    }
	
	$fn = md5( time() ) . '.' . $extn;;

    if (file_exists('../upld/'.$fn )) {
	
		 echo '<script>alert("THAT DOCUMENT ALREADY EXISTS"); window.location.href = "?ac=user";</script>';
		
        exit;	
		
    }
        
    if(move_uploaded_file($xfnt['tmp_name'], '../upld/'.$fn )) {
        
     $s = $con->exec("INSERT INTO upload (ct_id, u_id, file_desc, file, up_date) VALUES ('$xdep','". trim($_SESSION['id']) ."', '$xpnt', '$fn', now())");

	echo '<script>alert("SUCCESSFUL FILE UPLOAD"); window.location.href = "?ac=user";</script>';
		 
    }

}

/* update File */

$allow = array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx');

if(isset($_POST['re_file_name']) && isset($_FILES['newfile']) ){

	$rfn = $_POST['re_file_name'];
	$nf = $_FILES['newfile']; 
	
	$extn = pathinfo($nf['name'], PATHINFO_EXTENSION);
	
	if(!in_array(strtolower($extn), $allow)){
	
        echo '<script>alert("ONLY DOCUMENTS ALLOWED FOR UPLOAD"); window.location.href = "?ac=user";</script>';
		
        exit;
    }
	
	//$fn = md5( time() ) . '.' . $extn;;

    if (file_exists('../upld/'.$rfn )) {
        
		if(move_uploaded_file($nf['tmp_name'], '../upld/'.$rfn )) {
        
			$s = $con->exec("UPDATE upload SET up_date = now() WHERE file = 'rfn'");

			echo '<script>alert("FILE SUCCESSFULLY UPDATED"); window.location.href = "?ac=user";</script>';
		 
		}
		
		exit();
	
	}else{
	
		echo '<script>alert("ERROR IN FILE UPDATED"); window.location.href = "?ac=user";</script>';
		
		exit();
		
	}

}

/* del notice */
if(isset($_REQUEST['ndel'])){

$xnd = $_REQUEST['ndel'];

$xq = $con->query("SELECT * FROM upload WHERE file = '$xnd' LIMIT 1");

$r = $xq->fetch(PDO::FETCH_ASSOC); $ntd = $r['file']; 

$not_del = ('/../../upld/'.$ntd); 

if(file_exists($not_del)){
	unlink($not_del);
}

$cq = $con->exec("DELETE FROM upload WHERE file = '$xnd' LIMIT 1");

}

	include 'u_main.php';
	
	include 'inc/ft.php';
?>