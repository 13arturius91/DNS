/* js Custom */
$(document).ready(function () {

/* admin login */
$("#act").submit(function(e){

		e.preventDefault();
	
		$("#info").html("<img src='img/loader.gif'/>");	
		
		var pdact = $(this).serializeArray();
		
		$.ajax({
			url : "src/admlgn.php",
			type: "POST",
			data : pdact
		})	
		.done(function(data){ 
			$("#info").html(''+data+'');
		})
		.fail(function(data){
			$("#info").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'><i class= 'icon-remove'></i></button><i class='icon-ban-circle'></i><strong> Server Connection Failure </strong></div>");
		});	
	return false;
});
/* user login */
$("#lgn").submit(function(e){

		e.preventDefault();
	
		$("#login-info").html("<img src='img/loader.gif'/>");
		
		var pdlgn = $(this).serializeArray();
		
		$.ajax({	
			url : "src/ulgn.php",
			type: "POST",
			data : pdlgn			
		})
		.done(function(data){ 
			$("#login-info").html(''+data+'');
		})
		.fail(function(data){
			$("#login-info").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'><i class= 'icon-remove'></i></button><i class='icon-ban-circle'></i><strong> Server Connection Failure </strong></div>");
		});	
	return false;
});
/* new user {nwuser.php} */
$("#nwus").submit( function(e){

		e.preventDefault();
	
		$("#user-info").html("<img src='img/loader.gif'/>");
		
		var pdnws = $(this).serializeArray();
		
		$.ajax({
			url : "src/input/nwuser.php",
			type: "POST",
			data : pdnws
		})
		.done(function(data){ 
			$("#user-info").html(''+data+'');
		})
		.fail(function(data){
			$("#user-info").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'><i class= 'icon-remove'></i></button><i class='icon-ban-circle'></i><strong> Server Connection Failure </strong></div>");
		});	
	return false;
});
/* new admin {nwuser.php} */
$("#admus").submit( function(e){

		e.preventDefault();
	
		$("#adm-info").html("<img src='img/loader.gif'/>");
		
		var adnw = $(this).serializeArray();
		
		$.ajax({
			url : "src/input/nwadm.php",
			type: "POST",
			data : adnw
		})
		.done(function(data){ 
			$("#adm-info").html(''+data+'');
		})
		.fail(function(data){
			$("#adm-info").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'><i class= 'icon-remove'></i></button><i class='icon-ban-circle'></i><strong> Server Connection Failure </strong></div>");
		});	
	return false;
});
/* add departments */
$("#nwdep").submit(function(e){

		e.preventDefault();	
	
		$("#dep-info").html("<img src='img/loader.gif'/>");
		
		var pdct = $(this).serializeArray();
		
		$.ajax({
			url : "src/input/nwdep.php",
			type: "POST",
			data : pdct
		})	
		.done(function(data){ 
			$("#dep-info").html(''+data+'');
		})
		.fail(function(data){
			$("#dep-info").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'><i class= 'icon-remove'></i></button><i class='icon-ban-circle'></i><strong> Server Connection Failure </strong></div>");
		});	
	return false;
});
/* add privileges */
$("#nwpriv").submit(function(e){

		e.preventDefault();

		$("#priv-info").html("<img src='img/loader.gif'/>");
		
		var pdct = $(this).serializeArray();
		
		$.ajax({
			url : "src/input/nwpriv.php",
			type: "POST",
			data : pdct
		})	
		.done(function(data){ 
			$("#priv-info").html(''+data+'');
		})
		.fail(function(data){
			$("#priv-info").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'><i class= 'icon-remove'></i></button><i class='icon-ban-circle'></i><strong> Server Connection Failure </strong></div>");
		});	
	return false;
});
/* delete department  */
$(".delct").on('click',function(e){

	e.preventDefault();

	var tvid = $(this).attr("ct_id").toString();
	
	var tvnm = $(this).attr("ct_name").toString();
	
	console.log(tvid);
	
	if(confirm("Delete This Department :: " +tvnm+ " ?")){
	
		$.ajax({
			type: "POST",
			url: "src/admin.php",
			data : { dc: tvid }
		})	
		.done(function(html){ 
			$("#ct"+tvid).fadeIn('slow', function() {$(this).hide('slow');});
		})
		.fail(function(html){
			alert('Server Connection Failure');
		});	
	return false;
	
	}	
});

/* delete privileges */		
$(".delul").on('click',function(e){

	e.preventDefault();

	var ulid = $(this).attr("ul_id").toString();
	
	var ulnm = $(this).attr("ul_name").toString();
	
	console.log(ulid);
	
	if(confirm("Delete This User Level :: " +ulnm+ " ?")){
	
		$data = $.ajax({
			type: "POST",
			url: "admin.php",
			data : { du: ulid }
		})	
		.done(function(html){ 
			$("#cf"+ulid).fadeIn('slow', function() {$(this).hide('slow');});
		})
		.fail(function(html){
			alert('Server Connection Failure');
		});	
	return false;	
	}
});

/* delete user */
$(".btn btn-info btn-rounded").on('show.bs.modal',function(e){

	e.preventDefault();

	var u_id = $(this).attr("u_id").toString();
	
	var u_email = $(this).attr("u_email").toString();
	
	console.log(u_id);
	
	if(confirm("Delete This User Of Email :: " +u_email+ " ?")){
	
		$data = $.ajax({
			type: "POST",
			url: "src/admin.php",
			data : { udel: u_id }
		})	
		.done(function(html){ 
			$(".even"+u_id).fadeIn('slow', function() {$(this).hide('slow');});
		})
		.fail(function(html){
			alert('Server Connection Failure');
		});	
	return false;	
	}
});

/* delete notices (users)*/
$(".delpn").on('click',function(e){

	e.preventDefault();

	var xnd = $(this).attr("n_id").toString();
	
	var xtl = $(this).attr("n_title").toString();
	
	//console.log(xnd);
	
	if(confirm("ARE YOU SURE YOU WANT TO DELETE THIS NOTICE POST :: "+xtl+" ?")){
	
		$.ajax({
			type: "POST",
			url: "src/user.php",
			data : { ndel: xnd }
		})
		.done(function(html){ 
			$("[data-name='"+xtl+"']").fadeOut();
		})
		.fail(function(html){
			alert('Server Connection Failure');
		});	
	return false;	
	}	
});

/* delete notices (super admin & admin[head of department])*/
$(".delpnadm").on('click',function(e){

	e.preventDefault();

	var xntd = $(this).attr("not_id").toString();
	
	var xttl = $(this).attr("not_title").toString();
	
	//console.log(xnd);
	
	if(confirm("ARE YOU SURE YOU WANT TO DELETE THIS NOTICE POST :: "+xttl+" ?")){
	
		$.ajax({
			type: "POST",
			url: "src/admin.php",
			data : { delnt: xntd }
		})
		.done(function(html){ 
			$("[data-name='"+xttl+"']").fadeOut();
		})
		.fail(function(html){
			alert('Server Connection Failure');
		});	
	return false;	
	}	
});


});