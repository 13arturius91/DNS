<?php

$ac = isset( $_GET['ac'] ) ? $_GET['ac'] : "";

switch ( $ac ) {
	case 'user';
		usr();
	break;
	case 'adm';
		adm();
	break;
	case 'profile';
		pro();
	break;
	case 'u_profile';
		upro();
	break;
	case 'image';
		img();
	break;
	case 'u_image';
		uimg();
	break;	
	case 'login';
		lgn();
	break;
	case 'slogin';
		slgn();
	break;	
	case 'settings';
		stg();
	break;	
	case 'send_req';
		sendrq();
	break;
	case 'user_fp';
		userfp();
	break;
	case 'adm_fp';
		admfp();
	break;
	case 'send_pass_reset';
		sndprst();
	break;
	case 'rep_admin';
		repadmin();
	break;
	case 'rep_user';
		repuser();
	break;
	case 'del_user';
		deluser();
	break;
	case 'update_notice';
		upnotice();
	break;
	default:
		usr();
}

function usr(){
	$adm = 'src/user.php';
	if(file_exists($adm) ) { require 'src/user.php'; }else{ require 'src/404.php'; }
}
function adm(){
	$adm = 'src/admin.php';
	if(file_exists($adm) ) { require 'src/admin.php'; }else{ require 'src/404.php'; }
}
function pro(){
	$prf = 'src/profile.php';
	if(file_exists($prf) ){ require 'src/profile.php'; }else{ require 'src/404.php'; }
}
function upro(){
	$upro = 'src/u_profile.php';
	if(file_exists($upro) ){ require 'src/u_profile.php'; }else{ require 'src/404.php'; }
}
function img(){
	$img = 'src/image.php';
	if(file_exists($img) ){ require 'src/image.php'; }else{ require 'src/404.php'; }
}
function uimg(){
	$uimg = 'src/u_image.php';
	if(file_exists($uimg) ){ require 'src/u_image.php'; }else{ require 'src/404.php'; }
}
function lgn(){
	$lgn = 'src/ulgn.php';
	if(file_exists($lgn) ) { require 'src/ulgn.php'; }else{ require 'src/404.php'; }
}
function slgn(){
	$slgn = 'src/admlgn.php';
	if(file_exists($slgn) ){ require 'src/admlgn.php'; }else{ require 'src/404.php'; }
}
function stg(){
	$stg = 'src/sett.php';
	if(file_exists($stg) ){ require 'src/sett.php'; }else{ require 'src/404.php'; }
}
function sendrq(){
	$sendrq = 'src/send_req.php';
	if(file_exists($sendrq) ){ require 'src/send_req.php'; }else{ require 'src/404.php'; }
}
function userfp(){
	$usfp = 'src/user_fp.php';
	if(file_exists($usfp) ){ require 'src/user_fp.php'; }else{ require 'src/404.php'; }
}
function admfp(){
	$admfp = 'src/adm_fp.php';
	if(file_exists($admfp) ){ require 'src/adm_fp.php'; }else{ require 'src/404.php'; }
}
function sndprst(){
	$sndprst = 'src/send_pass_reset.php';
	if(file_exists($sndprst) ) { require 'src/send_pass_reset.php'; }else{ require 'src/404.php'; }
}
function repadmin(){
	$repad = 'src/rep_admin.php';
	if(file_exists($repad) ) { require 'src/rep_admin.php'; }else{ require 'src/404.php'; }
}
function repuser(){
	$repusr = 'src/rep_user.php';
	if(file_exists($repusr) ) { require 'src/rep_user.php'; }else{ require 'src/404.php'; }
}
function deluser(){
	$delusr = 'src/admin.php';
	if(file_exists($delusr) ) { require 'src/admin.php'; }else{ require 'src/404.php'; }
}
function upnotice(){
	$upnotice = 'src/user.php';
	if(file_exists($upnotice) ) { require 'src/user.php'; }else{ require 'src/404.php'; }
}
?>