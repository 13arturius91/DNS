<?php
include 'adm/cfg/cfg.php';
	
global $con;

date_default_timezone_set('Africa/Nairobi'); 
	
function adm($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));
	$adm = 'Administration';	
	$q = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$adm' AND up_date BETWEEN '$aweek' AND '$today'");
	$r = $q->fetch(PDO::FETCH_ASSOC);
	$ntf = $r["ntf"];
	if($ntf == 0){ echo '0'; }else{ echo $ntf; }

}
function ans($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$ans = 'ANZEN & Security';
	$q2 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$ans' AND up_date BETWEEN '$aweek' AND '$today'");
	$r2 = $q2->fetch(PDO::FETCH_ASSOC);
	$sna = $r2["ntf"];	
	if($sna == 0){ echo '0'; }else{ echo $sna; }
}
function bs($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$bs = 'Body Shop';
	$q3 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$bs' AND up_date BETWEEN '$aweek' AND '$today'");
	$r3 = $q3->fetch(PDO::FETCH_ASSOC);
	$sb = $r3["ntf"];
	if($sb == 0){ echo '0'; }else{ echo $sb; }
}
function bra($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$bra = 'Branches';
	$q4 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$bra' AND up_date BETWEEN '$aweek' AND '$today'");
	$r4 = $q4->fetch(PDO::FETCH_ASSOC);
	$arb = $r4["ntf"];
	if($arb == 0){ echo '0'; }else{ echo $arb; }
}
function bpg($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$bpg = 'Business Planning & Government';
	$q5 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$bpg' AND up_date BETWEEN '$aweek' AND '$today'");
	$r5 = $q5->fetch(PDO::FETCH_ASSOC);
	$gpb = $r5["ntf"];
	if($gpb == 0){ echo '0'; }else{ echo $gpb; }	
}
function csr($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$csr = 'CSR';
	$q6 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$csr' AND up_date BETWEEN '$aweek' AND '$today'");
	$r6 = $q6->fetch(PDO::FETCH_ASSOC);
	$rsc = $r6["ntf"];
	if($rsc == 0){ echo '0'; }else{ echo $rsc; }	
	
}
function cus($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$cr = 'Customer Relations';
	$q7 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$cr' AND up_date BETWEEN '$aweek' AND '$today'");
	$r7 = $q7->fetch(PDO::FETCH_ASSOC);
	$rc = $r7["ntf"];
	if($rc == 0){ echo '0'; }else{ echo $rc; }
	
}
function fin($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$fin = 'Finance';
	$q8 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$fin' AND up_date BETWEEN '$aweek' AND '$today'");
	$r8 = $q8->fetch(PDO::FETCH_ASSOC);
	$nif = $r8["ntf"]; 
	if($nif == 0){ echo '0'; }else{ echo $nif; }
}
function fms($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$fms = 'FMS';
	$q9 = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$fms' AND up_date BETWEEN '$aweek' AND '$today'");
	$r9 = $q9->fetch(PDO::FETCH_ASSOC);
	$msf = $r9["ntf"];
	if($msf == 0){ echo '0'; }else{ echo $msf; }
}
function hino($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$hino = 'Hino';
	$qa = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$hino' AND up_date BETWEEN '$aweek' AND '$today'");
	$ra = $qa->fetch(PDO::FETCH_ASSOC);
	$onih = $ra["ntf"];
	if($onih == 0){ echo '0'; }else{ echo $onih; }
}
function hr($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$hr = 'Human Resources';
	$qb = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$hr' AND up_date BETWEEN '$aweek' AND '$today'");
	$rb = $qb->fetch(PDO::FETCH_ASSOC);
	$rh = $rb["ntf"];
	if($rh == 0){ echo '0'; }else{ echo $rh; }
}
function it($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$it = 'IT';
	$qc = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$it' AND up_date BETWEEN '$aweek' AND '$today'");
	$rc = $qc->fetch(PDO::FETCH_ASSOC);
	$ti = $rc["ntf"];
	if($ti == 0){ echo '0'; }else{ echo $ti; }
}
function mdo($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$mdo = 'MDs Office';
	$qd = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$mdo' AND up_date BETWEEN '$aweek' AND '$today'");
	$rd = $qd->fetch(PDO::FETCH_ASSOC);
	$odm = $rd["ntf"];	
	if($odm == 0){ echo '0'; }else{ echo $odm; }
}
function oth($con){	
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$oth = 'Others';
	$qe = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$oth' AND up_date BETWEEN '$aweek' AND '$today'");
	$re = $qe->fetch(PDO::FETCH_ASSOC);
	$hto = $re["ntf"];
	if($hto == 0){ echo '0'; }else{ echo $hto; }	
}
function parts($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$par = 'parts';
	$qf = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$par' AND up_date BETWEEN '$aweek' AND '$today'");
	$rf = $qf->fetch(PDO::FETCH_ASSOC);
	$rap = $rf["ntf"];
	if($rap == 0){ echo '0'; }else{ echo $rap; }
}
function payr($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$pyr = 'Payroll';
	$qg = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$pyr' AND up_date BETWEEN '$aweek' AND '$today'");
	$rg = $qg->fetch(PDO::FETCH_ASSOC);
	$ryp = $rg["ntf"];
	if($ryp == 0){ echo '0'; }else{ echo $ryp; }
}
function serv($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$serv = 'Services';
	$qh = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$serv' AND up_date BETWEEN '$aweek' AND '$today'");
	$rh = $qh->fetch(PDO::FETCH_ASSOC);
	$vres = $rh["ntf"];
	if($vres == 0){ echo '0'; }else{ echo $vres; }
}
function tcap($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$tcap = 'TCAP';
	$qi = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$tcap' AND up_date BETWEEN '$aweek' AND '$today'");
	$ri = $qi->fetch(PDO::FETCH_ASSOC);
	$pact = $ri["ntf"];
	if($pact == 0){ echo '0'; }else{ echo $pact; }
}
function vs($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));		
	$vs = 'Vehicle Sales';
	$qj = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$vs' AND up_date BETWEEN '$aweek' AND '$today'");
	$rj = $qj->fetch(PDO::FETCH_ASSOC);
	$sv = $rj["ntf"];	
	if($sv == 0){ echo '0'; }else{ echo $sv; }
}
function yamaha($con){
	$today = date('Y-m-d H:i:s');	
	$aweek = date('Y-m-d H:i:s', strtotime('-14 day'));	
	$yah = 'Yamaha';
	$qk = $con->query("SELECT COUNT(nt_id) as ntf FROM upload INNER JOIN syscat ON upload.ct_id = syscat.ct_id WHERE cat = '$yah' AND up_date BETWEEN '$aweek' AND '$today'");
	$rk = $qk->fetch(PDO::FETCH_ASSOC);
	$hay = $rk["ntf"];
	if($hay == 0){ echo '0'; }else{ echo $hay; }
}
?>