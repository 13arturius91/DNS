<?php

include 'adm/cfg/cfg.php';

?>
<style>
@media screen and (max-height: 680px) {
	div.con {
        width: 1200px;
    }
}
div.con{
	position: relative;
    margin-right: 50px;
	margin:50px;
    float: left;
    padding: 10px;
	cursor:move;
	-webkit-perspective: 1000px;
}
div.dm{
	float: left;
	width: 280px;
	height: 140px;
	position: relative;
	padding: 10px;
	margin: 5px;
	background-color: white;
	color: black;
	cursor: pointer;
	border-radius: 4px;
	-webkit-animation: widget_preview 0.2s linear;
	-webkit-user-select: none;
            user-select: none;
    -webkit-box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.8);
            box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.8);
    -webkit-transform: rotateY(0deg);
            transform: rotateY(0deg);	
}
div.dm div.mn{
	height:85%;
    top: 0px;
    background-repeat: no-repeat;
    background-position: 50% 50%;
}
div.dm.unloaded {
    opacity: 0;
    -webkit-transform: rotateY(-90deg);     
}
div.dm.animation {
    -webkit-transition: opacity 0.3s, -webkit-transform 0.3s;       
}
</style>
<?php
if(isset($_GET['v'])){

	$ds = '';

	$v = $_GET['v'];
	
	$qx = $con->query("SELECT * FROM syscat INNER JOIN upload ON syscat.ct_id = upload.ct_id AND syscat.cat = '$v' ORDER BY up_date ASC");
	
	foreach($qx as $rx){
	
		$ds .='<a href="stats.php?stt='.$rx['file'].'"><div class="dm">
				<center><span style="color:blue;">'.$rx['up_date'].'</span></center>
				<div class="mn">'.$rx['file_desc'].'</div>
				</div></a>';
	
	}
?>
<div class="con">
<?php	
	echo $ds;
?>	
</div>
<?php	
}
?>				