# DNS - Digital Notification System 
It is a notice display system that replaces the tradition paper notice board to an Electronic Notice Board.

## It includes features such as:

1. Interactive User Area Notice Board
2. System User Console
3. Administrator Console
4. Super-Administrator Console

## Changelog

### v. 3.3.0.1 – August 24, 2015
* change in sql.db, slight bug fixes.

### v. 3.3.0 – August 17, 2015
* index.php style fixes, blink animation fix, user and admin user guide using intro.js.